<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 11/6/2017
 * Time: 2:27 PM
 */

abstract class ProductStatus extends BaseEnum {
    const Available = 0;
    const Purchased = 1;
    const Deleted = 2;
}