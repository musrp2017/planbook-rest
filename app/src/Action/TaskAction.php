<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/18/2017
 * Time: 5:48 PM
 */

namespace App\Action;


use App\DTO\JsonMessage;
use App\DTO\TaskDTO;
use App\Entity\Task;
use App\Entity\User;
use App\Resource\TaskResource;
use App\Resource\UserResource;
use App\Util\Utils;
use Doctrine\DBAL\Driver\PDOException;
use http\Exception;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class TaskAction
{
    /** @var UserResource userResource */
    private $userResource;

    /** @var TaskResource taskResource */
    private $taskResource;

    /** @var LoggerInterface logger */
    private $logger;

    /**
     * AuthAction constructor.
     * @param UserResource $userResource
     * @param TaskResource $taskResource
     * @param LoggerInterface $logger
     */
    public function __construct(UserResource $userResource, TaskResource $taskResource, LoggerInterface $logger)
    {
        $this->userResource = $userResource;
        $this->taskResource = $taskResource;
        $this->logger = $logger;

    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function getAllTasksByUsername($request, $response, $args)
    {
        try {
            //Verify that the user requested exists before attempting to pull associated tasks
            $user_id = $args['user_id'];
            /** @var User $user */
            $user = $this->userResource->getByUserId($user_id);
            if ($user) {
                try{
                    /** @var array $tasks */
                    $tasks = $this->taskResource->get($user->getUsername());
                    if($tasks){
                        return $response->withJson(array('tasks' => $tasks), 200);
                    }else{
                        return $response->withJson(array(), 200);
                    }
                }catch(InvalidArgumentException $e){
                    $data = new JsonMessage(false, $e->getMessage());
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            } else {
                //Could not find user
                $this->logger->info("Get All User Tasks Failure: Could not find the requested user_id. UserId='" . $user_id . "'");
                $data = new JsonMessage(false, 'UserId does not exist.');
                return $response->withJson($data->getArrayCopy(), 400);
            }
        }catch(Exception $e){
            $this->logger->error("Exception thrown in Getting all tasks by user attempt.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function getTaskByTaskId($request, $response, $args)
    {
        try {
            //Verify that the user requested exists before attempting to pull associated tasks
            $taskId = $args['task_id'];
            /** @var User $user */
            try{
                /** @var Task $task */
                $task = $this->taskResource->getByTaskId($taskId);
                return $response->withJson(array('task' => $task->getArrayCopy()), 200);
            }catch(InvalidArgumentException $e){
                $this->logger->error("Exception thrown in Getting task by id.", $e->getTrace());
                $data = new JsonMessage(false, $e->getMessage());
                return $response->withJson($data->getArrayCopy(), 400);
            }

        }catch(Exception $e){

            $this->logger->error("Exception thrown in Getting task by id.", $e->getMessage());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function createNewTask($request, $response, $args)
    {
        try {
            //fetch passed in UserId to verify that the User is valid
            $user_id = $args['user_id'];

            /** @var User $user */
            $user = $this->userResource->getByUserId($user_id);
            if ($user) {
                try{
                    //fetch all other params
                    /** @var string $title */
                    $requestTitle = $request->getParsedBodyParam('title');
                    $title = (string) $requestTitle;
                    if(!isset($title) || $title == null || $title == ''){
                        //if the title is not set
                        $message = "Error while creating Task. Task was missing a title.'";
                        $this->logger->error($message);
                        $data = new JsonMessage(false, $message);
                        return $response->withJson($data->getArrayCopy(), 400);
                    }else{
                        $message = $user->getUsername() .": creating new task '".$requestTitle."'.";
                        $this->logger->info($message);
                    }

                    $priority = $request->getParsedBodyParam('priority');
                    $description = $request->getParsedBodyParam('description');
                    /** @var array $deadline */
                    $deadline =  $request->getParsedBodyParam('deadline');

                    $deadlineDate = $deadline["date"];
                    $deadlineTimezone = $deadline["timezone"];

                    $completed = $request->getParsedBodyParam('completed');
                    $icon = $request->getParsedBodyParam('icon');
                    $order = $request->getParsedBodyParam('order');

                    //Create a TaskDTO and pass it to the TaskResource
                    /** @var TaskDTO $taskDTO */
                    $taskDTO = new TaskDTO();
                    $taskDTO->setHashCode(Utils::generateHash());
                    $taskDTO->setTitle($requestTitle);
                    $taskDTO->setUser($user);
                    $taskDTO->setPriority($priority);
                    $taskDTO->setDescription($description);
                    $taskDTO->setDeadline($deadlineDate, $deadlineTimezone);
                    $taskDTO->setCompleted($completed);
                    $taskDTO->setIcon($icon);
                    $taskDTO->setOrder($order);

                    try {
                        $taskDTO = $this->taskResource->createNew($taskDTO);
                    }catch(PDOException $e){
                        //a Doctrine Exception was thrown
                        $this->logger->error("Error while creating Task. '" .$e->getMessage() ."'");
                        $data = new JsonMessage(false, $e->getMessage());
                        return $response->withJson($data->getArrayCopy(), 400);
                    }
                    //Based on the updated success/message, respond accordingly
                    if ($taskDTO->getSuccess() == true) {
                        $this->logger->info("Successfully created Task: '" .$requestTitle. "'");
                        $task = $this->taskResource->getByHashCode($taskDTO->getHashCode());

                        return $response
                            ->withJson(
                                array(
                                    'task' => $task->getArrayCopy(),
                                    'isNewModel' => true
                                ), 200);
                    } else {
                        $this->logger->error("Error while creating Task. '" .$taskDTO->getMessage() ."'");
                        $data = new JsonMessage(false, $taskDTO->getMessage());
                        return $response->withJson($data->getArrayCopy(), 400);
                    }

                }catch(InvalidArgumentException $e){
                    $data = new JsonMessage(false, $e->getMessage());
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            } else {
                //Could not find user
                $this->logger->info("Create Task Failure: Could not find the requested user_id. UserId='" . $user_id . "'");
                $data = new JsonMessage(false, 'UserId does not exist.');
                return $response->withJson($data->getArrayCopy(), 400);
            }
        } catch (Exception $e){
            $this->logger->error("Error while creating Task.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function updateTask($request, $response, $args)
    {
        try {
            //fetch passed in UserId to verify that the User is valid
            $taskId = $args['task_id'];

            /** @var Task $task */
            $task = $this->taskResource->getByTaskId($taskId);
            if ($task) {
                try{
                    //fetch all other params
                    $title = $request->getParsedBodyParam('title');
                    $priority = $request->getParsedBodyParam('priority');
                    $description = $request->getParsedBodyParam('description');
                    /** @var array $deadline */
                    $deadline =  $request->getParsedBodyParam('deadline');

                    $deadlineDate = $deadline["date"];
                    $deadlineTimezone = $deadline["timezone"];
                    $completed = $request->getParsedBodyParam('completed');
                    $icon = $request->getParsedBodyParam('icon');
                    $hash_code = $request->getParsedBodyParam('hash_code');

                    //Create a TaskDTO and pass it to the TaskResource
                    /** @var TaskDTO $taskDTO */
                    $taskDTO = new TaskDTO();
                    $taskDTO->setId($taskId);
                    $taskDTO->setTitle($title);
                    $taskDTO->setPriority($priority);
                    $taskDTO->setDescription($description);
                    $taskDTO->setDeadline($deadlineDate, $deadlineTimezone);
                    $taskDTO->setCompleted($completed);
                    $taskDTO->setIcon($icon);
                    $taskDTO->setHashCode($hash_code);

                    $taskDTO = $this->taskResource->updateTask($taskDTO);

                    //Based on the updated success/message, respond accordingly
                    if ($taskDTO->getSuccess() == true) {
                        $this->logger->info("Successfully created Task: '" .$title. "'");
                        $task = $this->taskResource->getByHashCode($taskDTO->getHashCode());

                        return $response->withJson(array('task' => $task->getArrayCopy()), 200);
                    } else {
                        $this->logger->error("Error while creating Task. '" .$taskDTO->getMessage() ."'");
                        $data = new JsonMessage(false, $taskDTO->getMessage());
                        return $response->withJson($data->getArrayCopy(), 400);
                    }

                }catch(InvalidArgumentException $e){
                    $data = new JsonMessage(false, $e->getMessage());
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            } else {
                //Could not find task
                $this->logger->info("Update Task Failure: Could not find the requested task. TaskId='" . $taskId . "'");
                $data = new JsonMessage(false, 'Task does not exist.');
                return $response->withJson($data->getArrayCopy(), 400);
            }
        } catch (Exception $e){
            $this->logger->error("Error while updating Task.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function removeTask($request, $response, $args)
    {
        try {
            //fetch passed in taskId to verify that the Task is valid
            $taskId = $args['task_id'];

            /** @var Task $task */
            $task = $this->taskResource->getByTaskId($taskId);
            if ($task) {
                try{
                    $taskDTO = new TaskDTO();
                    $taskDTO->setId($taskId);
                    $taskDTO = $this->taskResource->deleteTask($taskDTO);

                    //Based on the updated success/message, respond accordingly
                    if ($taskDTO->getSuccess() == true) {
                        $this->logger->info("Successfully removed Task: '" .$taskId. "'");
                        $data = new JsonMessage(true, $taskDTO->getMessage());
                        return $response->withJson($data->getArrayCopy(), 200);
                    } else {
                        $this->logger->error("Error while creating Task. '" .$taskDTO->getMessage() ."'");
                        $data = new JsonMessage(false, $taskDTO->getMessage());
                        return $response->withJson($data->getArrayCopy(), 400);
                    }

                }catch(InvalidArgumentException $e){
                    $data = new JsonMessage(false, $e->getMessage());
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            } else {
                //Could not find task
                $this->logger->info("Update Task Failure: Could not find the requested task. TaskId='" . $taskId . "'");
                $data = new JsonMessage(false, 'Task does not exist.');
                return $response->withJson($data->getArrayCopy(), 400);
            }
        } catch (Exception $e){
            $this->logger->error("Error while updating Task.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }


}