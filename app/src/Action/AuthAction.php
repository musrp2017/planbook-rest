<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/14/2017
 * Time: 1:34 AM
 */

namespace App\Action;

use App\DTO\JsonMessage;
use App\DTO\UserDTO;
use App\Entity\User;
use App\Resource\UserResource;
use App\Util\Utils;
use Exception;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class AuthAction
{
    /** @var UserResource userResource */
    private $userResource;
    private $logger;

    /**
     * AuthAction constructor.
     * @param UserResource $userResource
     * @param LoggerInterface $logger
     */
    public function __construct(UserResource $userResource, LoggerInterface $logger)
    {
        $this->userResource = $userResource;
        $this->logger = $logger;

    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function checkAuthStatus($request, $response, $args)
    {
        try {
            $userId = $request->getHeader('user_id');

            /** @var User $user */
            $user = $this->userResource->get($userId);
            if ($user) {
                $requestAuthToken = $request->getHeader('auth_token');
                if($user->getAuthToken() == $requestAuthToken){
                    return $response->withJson(array('user' => $user->getArrayCopy(), 'success' => true, 'message' => 'User has a valid session.'), 200);
                }
            }else{
                $data = new JsonMessage(false, "Client has no valid login cookies.");
                return $response->withJson($data->getArrayCopy(), 200);
            }
        }catch(Exception $e){
            $this->logger->error("Exception thrown in verifying authentication status.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function logInUser($request, $response, $args)
    {
        try {
            $username = $request->getParsedBodyParam('username');

            /** @var User $user */
            $user = $this->userResource->getByUsername($username);
            if ($user) {
                //compare the posted user password with the stored copy
                $requestPassword = $request->getParsedBodyParam('password');
                if ($user->getPassword() == $requestPassword) {
                    //Authentication Success!
                    $this->logger->info("Login Success: Username='" . $username . "'");

                    return $response->withJson(array('user' => $user->getArrayCopy()), 200)
                        ->withAddedHeader('user_id', $user->getId())
                        ->withAddedHeader('auth_token', $user->getAuthToken());
                } else {
                    //Invalid Credentials
                    $this->logger->info("Login Failure: Invalid Username/Password. Username='" . $username . "'");
                    $data = new JsonMessage(false, 'Invalid username or password.');
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            } else {
                //Could not find user
                $this->logger->info("Login Failure: Could not find the requested username. Username='" . $username . "'");
                $data = new JsonMessage(false, 'Username does not exist.');
                return $response->withJson($data->getArrayCopy(), 400);
            }
        }catch(Exception $e){
            $this->logger->error("Exception thrown in Login attempt.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function signUp($request, $response, $args)
    {
        try {
            //Check if the Username is taken
            $username = $request->getParsedBodyParam('username');
            $user = $this->userResource->getByUsername($username);
            if ($user) {
                return $response->withStatus(400, 'Username already exists');
            } else {
                //fetch other passed in params
                $email = $request->getParsedBodyParam('email');
                $unencryptedPassword = $request->getParsedBodyParam('password');

                //Create a UserDTO and pass it to the UserResource
                /** @var UserDTO $userDTO */
                $userDTO = new UserDTO(Utils::generateHash());
                $userDTO->setUsername($username);
                $userDTO->setPassword($unencryptedPassword);
                $userDTO->setEmail($email);
                $userDTO->setAuthToken(Utils::generateHash());

                $userDTO = $this->userResource->createNew($userDTO);


                //Based on the updated success/message, respond accordingly
                if ($userDTO->getSuccess() == true) {
                    $this->logger->info("Successfully created User: '" .$username. "'");
                    $user = $this->userResource->getByUsername($username);

                    return $response->withJson(array('user' => $user->getArrayCopy()), 200);
                } else {
                    $this->logger->error("Error while creating User. '" .$userDTO->getMessage() ."'");
                    $data = new JsonMessage(false, $userDTO->getMessage());
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            }
        }catch (Exception $e){
            $this->logger->error("Error while creating User.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function logoutUser($request, $response, $args)
    {
        $data = new JsonMessage(true, 'User Successfully logged out.');
        return $response->withJson($data->getArrayCopy(), 200);
    }






}