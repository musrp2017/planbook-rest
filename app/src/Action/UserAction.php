<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/13/2017
 * Time: 9:06 PM
 */

namespace App\Action;

use App\Entity\User;
use App\Resource\UserResource;
use Doctrine\Common\Collections\Collection;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;


final class UserAction
{
    private $userResource;
    private $logger;

    /**
     * UserAction constructor.
     * @param UserResource $userResource
     * @param LoggerInterface $logger
     */
    public function __construct(UserResource $userResource, LoggerInterface $logger)
    {
        $this->userResource = $userResource;
        $this->logger = $logger;

    }

    /**
     * @param $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function fetch($request, $response, $args)
    {
        /** @var User $user */
        $user = $this->userResource->get();
        return $response->withJson($user->getArrayCopy(), 200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function fetchOne($request, $response, $args)
    {
        /** @var User $user */
        $user = $this->userResource->get($args['username']);
        if ($user) {
            return $response->withJson($user->getArrayCopy(), 200);
        }
        return $response->withStatus(404, 'No user found with that username.');
    }

}