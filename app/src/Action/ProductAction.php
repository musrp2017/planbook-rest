<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/18/2017
 * Time: 5:48 PM
 */

namespace App\Action;


use App\DTO\JsonMessage;
use App\DTO\ProductDTO;
use App\Entity\Product;
use App\Entity\User;
use App\Resource\ProductResource;
use App\Resource\UserResource;
use App\Util\Utils;
use Doctrine\DBAL\Driver\PDOException;
use http\Exception;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class ProductAction
{
    /** @var UserResource userResource */
    private $userResource;

    /** @var ProductResource productResource */
    private $productResource;

    /** @var LoggerInterface logger */
    private $logger;

    /**
     * AuthAction constructor.
     * @param UserResource $userResource
     * @param ProductResource $productResource
     * @param LoggerInterface $logger
     */
    public function __construct(UserResource $userResource, ProductResource $productResource, LoggerInterface $logger)
    {
        $this->userResource = $userResource;
        $this->productResource = $productResource;
        $this->logger = $logger;

    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function getAllProductsByUsername($request, $response, $args)
    {
        try {
            //Verify that the user requested exists before attempting to pull associated Products
            $user_id = $args['user_id'];
            /** @var User $user */
            $user = $this->userResource->getByUserId($user_id);
            if ($user) {
                try{
                    /** @var array $products */
                    $products = $this->productResource->get($user->getUsername());
                    if($products){
                        return $response->withJson(array('products' => $products), 200);
                    }else{
                        return $response->withJson(array(), 200);
                    }
                }catch(InvalidArgumentException $e){
                    $data = new JsonMessage(false, $e->getMessage());
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            } else {
                //Could not find user
                $this->logger->info("Get All User Products Failure: Could not find the requested user_id. UserId='" . $user_id . "'");
                $data = new JsonMessage(false, 'UserId does not exist.');
                return $response->withJson($data->getArrayCopy(), 400);
            }
        }catch(Exception $e){
            $this->logger->error("Exception thrown in Getting all products by user attempt.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function getProductByProductId($request, $response, $args)
    {
        try {
            //Verify that the user requested exists before attempting to pull associated Products
            $productId = $args['product_id'];
            /** @var User $user */
            try{
                /** @var Product $product */
                $product = $this->productResource->getByProductId($productId);
                return $response->withJson(array('product' => $product->getArrayCopy()), 200);
            }catch(InvalidArgumentException $e){
                $this->logger->error("Exception thrown in Getting product by id.", $e->getTrace());
                $data = new JsonMessage(false, $e->getMessage());
                return $response->withJson($data->getArrayCopy(), 400);
            }

        }catch(Exception $e){

            $this->logger->error("Exception thrown in Getting product by id.", $e->getMessage());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function createNewProduct($request, $response, $args)
    {
        try {
            //fetch passed in UserId to verify that the User is valid
            $user_id = $args['user_id'];

            /** @var User $user */
            $user = $this->userResource->getByUserId($user_id);
            if ($user) {
                try{
                    //fetch all other params
                    /** @var string $title */
                    $requestTitle = $request->getParsedBodyParam('title');
                    $title = (string) $requestTitle;
                    if(!isset($title) || $title == null || $title == ''){
                        //if the title is not set
                        $message = "Error while creating Product. Product was missing a title.'";
                        $this->logger->error($message);
                        $data = new JsonMessage(false, $message);
                        return $response->withJson($data->getArrayCopy(), 400);
                    }else{
                        $message = $user->getUsername() .": creating new product '".$requestTitle."'.";
                        $this->logger->info($message);
                    }

                    $price = $request->getParsedBodyParam('price');
                    $description = $request->getParsedBodyParam('description');

                    $created =  $request->getParsedBodyParam('created');

                    $status = $request->getParsedBodyParam('status');
                    $imageSourcePath = $request->getParsedBodyParam('imageSourcePath');

                    //Create a ProductDTO and pass it to the ProductResource
                    /** @var ProductDTO $productDTO */
                    $productDTO = new ProductDTO();
                    $productDTO->setHashCode(Utils::generateHash());
                    $productDTO->setTitle($requestTitle);
                    $productDTO->setUser($user);
                    $productDTO->setPrice($price);
                    $productDTO->setDescription($description);
                    $productDTO->setCreated($created);
                    $productDTO->setStatus($status);
                    $productDTO->setImageSourcePath($imageSourcePath);

                    try {
                        $productDTO = $this->productResource->createNew($productDTO);
                    }catch(PDOException $e){
                        //a Doctrine Exception was thrown
                        $this->logger->error("Error while creating Product. '" .$e->getMessage() ."'");
                        $data = new JsonMessage(false, $e->getMessage());
                        return $response->withJson($data->getArrayCopy(), 400);
                    }
                    //Based on the updated success/message, respond accordingly
                    if ($productDTO->getSuccess() == true) {
                        $this->logger->info("Successfully created Product: '" .$requestTitle. "'");
                        $product = $this->productResource->getByHashCode($productDTO->getHashCode());

                        return $response
                            ->withJson(
                                array(
                                    'product' => $product->getArrayCopy(),
                                    'isNewModel' => true
                                ), 200);
                    } else {
                        $this->logger->error("Error while creating Product. '" .$productDTO->getMessage() ."'");
                        $data = new JsonMessage(false, $productDTO->getMessage());
                        return $response->withJson($data->getArrayCopy(), 400);
                    }

                }catch(InvalidArgumentException $e){
                    $data = new JsonMessage(false, $e->getMessage());
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            } else {
                //Could not find user
                $this->logger->info("Create Product Failure: Could not find the requested user_id. UserId='" . $user_id . "'");
                $data = new JsonMessage(false, 'UserId does not exist.');
                return $response->withJson($data->getArrayCopy(), 400);
            }
        } catch (Exception $e){
            $this->logger->error("Error while creating Product.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return string
     */
    public function updateProduct($request, $response, $args)
    {
        try {
            //fetch passed in UserId to verify that the User is valid
            $productId = $args['product_id'];

            /** @var Product $product */
            $product = $this->productResource->getByProductId($productId);
            if ($product) {
                try{
                    //fetch all other params
                    $title = $request->getParsedBodyParam('title');
                    $price = $request->getParsedBodyParam('price');
                    $description = $request->getParsedBodyParam('description');

                    $status = $request->getParsedBodyParam('status');
                    $imageSourcePath = $request->getParsedBodyParam('imageSourcePath');
                    $hash_code = $request->getParsedBodyParam('hash_code');

                    //Create a ProductDTO and pass it to the ProductResource
                    /** @var ProductDTO $productDTO */
                    $productDTO = new ProductDTO();
                    $productDTO->setId($productId);
                    $productDTO->setTitle($title);
                    $productDTO->setDescription($description);
                    $productDTO->setPrice($price);

                    $productDTO->setStatus($status);
                    $productDTO->setImageSourcePath($imageSourcePath);
                    $productDTO->setHashCode($hash_code);

                    $productDTO = $this->productResource->updateProduct($productDTO);

                    //Based on the updated success/message, respond accordingly
                    if ($productDTO->getSuccess() == true) {
                        $this->logger->info("Successfully created Product: '" .$title. "'");
                        $product = $this->productResource->getByHashCode($productDTO->getHashCode());

                        return $response->withJson(array('product' => $product->getArrayCopy()), 200);
                    } else {
                        $this->logger->error("Error while creating Product. '" .$productDTO->getMessage() ."'");
                        $data = new JsonMessage(false, $productDTO->getMessage());
                        return $response->withJson($data->getArrayCopy(), 400);
                    }

                }catch(InvalidArgumentException $e){
                    $data = new JsonMessage(false, $e->getMessage());
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            } else {
                //Could not find Product
                $this->logger->info("Update Product Failure: Could not find the requested product. ProductId='" . $productId . "'");
                $data = new JsonMessage(false, 'Product does not exist.');
                return $response->withJson($data->getArrayCopy(), 400);
            }
        } catch (Exception $e){
            $this->logger->error("Error while updating Product.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function removeProduct($request, $response, $args)
    {
        try {
            //fetch passed in productId to verify that the Product is valid
            $productId = $args['product_id'];

            /** @var Product $product */
            $product = $this->productResource->getByProductId($productId);
            if ($product) {
                try{
                    $productDTO = new ProductDTO();
                    $productDTO->setId($productId);
                    $productDTO = $this->productResource->deleteProduct($productDTO);

                    //Based on the updated success/message, respond accordingly
                    if ($productDTO->getSuccess() == true) {
                        $this->logger->info("Successfully removed Product: '" .$productId. "'");
                        $data = new JsonMessage(true, $productDTO->getMessage());
                        return $response->withJson($data->getArrayCopy(), 200);
                    } else {
                        $this->logger->error("Error while creating Product. '" .$productDTO->getMessage() ."'");
                        $data = new JsonMessage(false, $productDTO->getMessage());
                        return $response->withJson($data->getArrayCopy(), 400);
                    }

                }catch(InvalidArgumentException $e){
                    $data = new JsonMessage(false, $e->getMessage());
                    return $response->withJson($data->getArrayCopy(), 400);
                }
            } else {
                //Could not find product
                $this->logger->info("Update Product Failure: Could not find the requested product. ProductId='" . $productId . "'");
                $data = new JsonMessage(false, 'Product does not exist.');
                return $response->withJson($data->getArrayCopy(), 400);
            }
        } catch (Exception $e){
            $this->logger->error("Error while updating Product.", $e->getTrace());
            $data = new JsonMessage(false, 'System encountered a fatal error. Please try again in a few minutes.');
            return $response->withJson($data->getArrayCopy(), 400);
        }
    }


}