<?php
namespace App\Action;

use Slim\Csrf\Guard;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use Psr\Log\LoggerInterface;

final class HomeAction
{
    private $view;
    private $logger;

    /**
     * HomeAction constructor.
     * @param Twig $view
     * @param LoggerInterface $logger
     */
    public function __construct(Twig $view, LoggerInterface $logger)
    {
        $this->view = $view;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function dispatch($request, $response, $args)
    {
        $this->logger->info("Home page action dispatched");


        $this->view->render($response, 'home.twig');



        return $response;

    }
}
