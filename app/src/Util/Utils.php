<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/14/2017
 * Time: 2:34 PM
 */

namespace App\Util;

class Utils
{

    /**
     * Generate a unique hash
     * @return String
     */
    static final public function generateHash() {
        $hash = sha1(date('dmYHis') . rand(10000, 99999));
        return substr($hash, 0, 8) . '-' . substr($hash, 9, 4) . '-' . substr($hash, 14, 4) . '-' . substr($hash, 19, 4) . '-' . substr($hash, 24, 12);
    }


    /**
     * return a DateTime Object with current time
     * @return \DateTime
     */
    static final public function getCurrentDateAndTime() {
        return new \DateTime(date('d-m-Y H:i:s'));
    }

}