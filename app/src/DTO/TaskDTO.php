<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/18/2017
 * Time: 5:43 PM
 */

namespace App\DTO;


use App\Entity\User;
use DateInterval;
use DateTime;
use Exception;

class TaskDTO
{
    private $id = NULL;
    private $user = NULL;

    private $icon = NULL;
    private $deadline = NULL;
    private $priority = NULL;
    private $title = NULL;
    private $description = NULL;
    private $completed = NULL;
    private $hash_code = NULL;
    private $order = NULL;

    private $success = NULL;
    private $message = NULL;

    public function __construct(){
    }

    /**
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param integer $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }



    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        if($this->icon == null){
            return "";
        }
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return \datetime
     */
    public function getDeadline()
    {
        if($this->deadline == null){
            $newDeadline = new DateTime();
            $newDeadline->add(new DateInterval('P7D')); //Set the deadline to one week from now
            return $newDeadline;
        }
        return $this->deadline;
    }

    /**
     * @param $deadlineDate
     * @param $deadlineTimezone
     */
    public function setDeadline($deadlineDate = null, $deadlineTimezone = null)
    {
        if($deadlineDate == null || $deadlineTimezone == null | $deadlineDate == "" || $deadlineTimezone == ""){
            return;
        }
        try{
            $deadline = new DateTime($deadlineDate, $deadlineTimezone);
            $this->deadline = $deadline;
        }catch(Exception $e){
            return;
        }
    }

    /**
     * @return string
     */
    public function getPriority()
    {
        if($this->priority == null){
            return "Low";
        }
        return $this->priority;
    }

    /**
     * @param string $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        if($this->description == null){
            return "";
        }
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return boolean
     */
    public function getCompleted()
    {
        if($this->completed == null){
            return false;
        }
        return $this->completed;
    }

    /**
     * @param boolean $completed
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getHashCode()
    {
        return $this->hash_code;
    }

    /**
     * @param string $hash_code
     */
    public function setHashCode($hash_code)
    {
        $this->hash_code = $hash_code;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param boolean $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }



}