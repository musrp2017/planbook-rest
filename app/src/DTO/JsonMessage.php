<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/16/2017
 * Time: 3:56 PM
 */

namespace App\DTO;


class JsonMessage
{
    private $success = NULL;
    private $message = NULL;

    /**
     * JsonMessage constructor.
     * @param boolean $success
     * @param string $message
     */
    public function __construct($success, $message)
    {
        $this->success = $success;
        $this->message = $message;

        return $this;
    }


    /**
     * @return null
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param null $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param null $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }




    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}