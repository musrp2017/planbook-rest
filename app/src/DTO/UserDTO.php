<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/14/2017
 * Time: 2:37 PM
 */

namespace App\DTO;

class UserDTO
{
    private $id = NULL;
    private $username = NULL;
    private $email = NULL;
    private $password = NULL;
    private $auth_token = NULL;
    private $hash_code = NULL;

    private $success = NULL;
    private $message = NULL;

    /**
     * UserDTO constructor.
     * @param $hash_code
     */
    public function __construct($hash_code){
        $this->hash_code = $hash_code;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param null $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param null $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return null
     */
    public function getAuthToken()
    {
        return $this->auth_token;
    }

    /**
     * @param null $auth_token
     */
    public function setAuthToken($auth_token)
    {
        $this->auth_token = $auth_token;
    }

    /**
     * @return null
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param null $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return null
     */
    public function getHashCode()
    {
        return $this->hash_code;
    }

    /**
     * @param null $hash_code
     */
    public function setHashCode($hash_code)
    {
        $this->hash_code = $hash_code;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }





}