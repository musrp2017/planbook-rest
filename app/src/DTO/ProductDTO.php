<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/18/2017
 * Time: 5:43 PM
 */

namespace App\DTO;


use App\Entity\User;
use DateInterval;
use DateTime;
use Exception;
use ProductStatus;

class ProductDTO
{
    private $id = NULL;
    private $user = NULL;
    private $imageSourcePath = NULL;
    private $created = NULL;
    private $title = NULL;
    private $description = NULL;
    private $hash_code = NULL;
    private $price = NULL;
    private $status = NULL;

    private $success = NULL;
    private $message = NULL;

    public function __construct(){
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param null $status
     */
    public function setStatus($status)
    {
        if(ProductStatus::isValidName($status)){
            $this->status = $status;
        }
        if(ProductStatus::isValidValue($status)){
            $this->status = $status;
        }
    }


    /**
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param integer $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getImageSourcePath()
    {
        if($this->imageSourcePath == null){
            return "";
        }
        return $this->imageSourcePath;
    }

    /**
     * @param string $imageSourcePath
     */
    public function setImageSourcePath($imageSourcePath)
    {
        $this->imageSourcePath = $imageSourcePath;
    }

    /**
     * @return \datetime
     */
    public function getCreated()
    {
        if($this->created == null){
            $newCreated = new DateTime();
            return $newCreated;
        }
        return $this->created;
    }

    /**
     * @param $createdDate
     */
    public function setCreated($createdDate = null)
    {
        if ($createdDate == null || $createdDate == "") {
            return;
        }
        try {
            $deadline = new DateTime($createdDate);
            $this->created = $deadline;
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        if($this->description == null){
            return "";
        }
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getHashCode()
    {
        return $this->hash_code;
    }

    /**
     * @param string $hash_code
     */
    public function setHashCode($hash_code)
    {
        $this->hash_code = $hash_code;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param boolean $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }



}