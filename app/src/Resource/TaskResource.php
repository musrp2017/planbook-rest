<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/13/2017
 * Time: 9:04 PM
 */

namespace App\Resource;

use App\AbstractResource;
use App\DTO\TaskDTO;
use App\DTO\UserDTO;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Driver\PDOException;
use Psr\Log\InvalidArgumentException;

/**
 * Class Resource
 * @package App\Resource
 */
class TaskResource extends AbstractResource
{
    /**
     * @param string|null $username
     *
     * @param bool $getOnlyActive
     * @return array|InvalidArgumentException
     */
    public function get($username = null, $getOnlyActive = true)
    {
        if ($username === null) {
            //Return Error Response: Need username to lookup tasks
            return new InvalidArgumentException("Param 'username' must not be null");
        } else {
            /** @var User $user */
            $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
                array('username' => $username)
            );
            if ($user) {
                if($getOnlyActive){
                    $tasks = $this->entityManager->getRepository('App\Entity\Task')->findBy(
                        array(
                            'user' => $user->getId(),
                            'active' => true
                        )
                    );

                }else{
                    $tasks = $this->entityManager->getRepository('App\Entity\Task')->findBy(
                        array(
                            'user' => $user->getId(),
                            'active' => true
                        )
                    );
                }

                if($tasks){
                    $tasks = array_map(
                        function ($task) {
                            /** @var Task $task */
                            return $task->getArrayCopy();
                        },
                        $tasks
                    );
                    return $tasks;
                }else{
                    //No Tasks were found, so return an empty array
                    return array();
                }

            }else{
                //Return Error response: Could not find username specified
                throw new InvalidArgumentException("Param 'username' does not exist.");
            }
        }
    }

    /**
     * @param null $taskId
     * @param bool $getOnlyActive
     * @return Task|InvalidArgumentException
     */
    public function getByTaskId($taskId = null, $getOnlyActive = true)
    {
        /** @var Task $task */
        if($getOnlyActive){
            $task = $this->entityManager->getRepository('App\Entity\Task')->findOneBy(
                array(
                    'id' => $taskId,
                    'active' => true
                )
            );
        }else{
            $task = $this->entityManager->getRepository('App\Entity\Task')->findOneBy(
                array(
                    'id' => $taskId
                )
            );
        }

        if ($task) {
            return $task;
        }
        //Return error response: Could not find task id specified
        throw new InvalidArgumentException("Param 'taskId' does not exist.");
    }

    /**
     * @param null $hashCode
     * @param bool $getOnlyActive
     * @return Task|InvalidArgumentException
     */
    public function getByHashCode($hashCode = null, $getOnlyActive = true)
    {
        if($getOnlyActive){
            $task = $this->entityManager->getRepository('App\Entity\Task')->findOneBy(
                array(
                    'hash_code' => $hashCode,
                    'active' => true
                )
            );
        }else{
            /** @var Task $task */
            $task = $this->entityManager->getRepository('App\Entity\Task')->findOneBy(
                array('hash_code' => $hashCode)
            );
        }

        if ($task) {
            return $task;
        }
        //Return error response: Could not find task id specified
        throw new InvalidArgumentException("Param 'hash_code' does not exist.");
    }

    /**
     * @param TaskDTO $taskDTO
     * @return TaskDTO
     */
    public function createNew(TaskDTO $taskDTO)
    {
        //Create a new Task using the values from the TaskDTO
        /** @var Task $newTask */
        $newTask = new Task($taskDTO->getHashCode());
        if($taskDTO->getUser() != null){
            $newTask->setUser($taskDTO->getUser());
        }

        if($taskDTO->getCompleted()){
            $newTask->setCompleted(true);
        }else{
            $newTask->setCompleted(false);
        }

        if($taskDTO->getDeadline() != null){
            $newTask->setDeadline($taskDTO->getDeadline());
        }

        if($taskDTO->getDescription() != null){
            $newTask->setDescription($taskDTO->getDescription());
        }else{
            $newTask->setDescription("");
        }

        if($taskDTO->getIcon() != null){
            $newTask->setIcon($taskDTO->getIcon());
        }

        if($taskDTO->getPriority() != null){
            $newTask->setPriority($taskDTO->getPriority());
        }

        if($taskDTO->getTitle() != null){
            $newTask->setTitle($taskDTO->getTitle());
        }

        if($taskDTO->getOrder() != null){
            $newTask->setOrder($taskDTO->getOrder());
        }else{
            $newTask->setOrder(0);
        }

        $newTask->setActive(true);

        //Persist in the Entity Manager
        $this->entityManager->persist($newTask);
        $this->entityManager->flush();

        //Check if the persistence was successful
        /** @var Task $persistedTaskObj */
        $persistedTaskObj = $this->entityManager->getRepository('App\Entity\Task')->findOneBy(
            array('hash_code' => $taskDTO->getHashCode())
        );
        if($persistedTaskObj){
            $taskDTO->setSuccess(true);
            $taskDTO->setMessage("Successfully saved the new Task.");
            return $taskDTO;
        }else{
            $taskDTO->setSuccess(false);
            $taskDTO->setMessage("Failed to persist the task to the database.");
            return $taskDTO;
        }
    }
 /**
     * @param TaskDTO $taskDTO
     * @return TaskDTO
     */
    public function updateTask(TaskDTO $taskDTO)
    {
        //Check if the persistence was successful

        /** @var Task $task */
        $task = $this->entityManager->getRepository('App\Entity\Task')->findOneBy(
            array('id' => $taskDTO->getId())
        );
        if($task){
            //Create an updated Task using the values from the TaskDTO
            /** @var Task $task */
            $task->setUser($taskDTO->getUser());
            $task->setCompleted($taskDTO->getCompleted());
            $task->setDeadline($taskDTO->getDeadline());
            $task->setDescription($taskDTO->getDescription());
            $task->setIcon($taskDTO->getIcon());
            $task->setPriority($taskDTO->getPriority());
            $task->setTitle($taskDTO->getTitle());

            //Persist in the Entity Manager
            $this->entityManager->persist($task);
            $this->entityManager->flush();

            $taskDTO->setSuccess(true);
            $taskDTO->setMessage("Successfully saved the updated Task.");
            return $taskDTO;
        }else{
            $taskDTO->setSuccess(false);
            $taskDTO->setMessage("Failed to persist the task to the database.");
            return $taskDTO;
        }
    }

    /**
     * @param TaskDTO $taskDTO
     * @return TaskDTO
     */
    public function deleteTask(TaskDTO $taskDTO)
    {
        //Check if the persistence was successful

        /** @var Task $task */
        $task = $this->entityManager->getRepository('App\Entity\Task')->findOneBy(
            array('id' => $taskDTO->getId())
        );
        if($task){

            $task->setActive(false);

            //Persist in the Entity Manager
            $this->entityManager->persist($task);
            $this->entityManager->flush();

            $taskDTO->setSuccess(true);
            $taskDTO->setMessage("Successfully removed the Task.");
            return $taskDTO;
        }else{
            $taskDTO->setSuccess(false);
            $taskDTO->setMessage("Failed to persist the task to the database.");
            return $taskDTO;
        }
    }

}