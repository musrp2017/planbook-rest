<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/13/2017
 * Time: 9:04 PM
 */

namespace App\Resource;

use App\AbstractResource;
use App\DTO\UserDTO;
use App\Entity\User;
use http\Exception\InvalidArgumentException;

/**
 * Class Resource
 * @package App
 */
class UserResource extends AbstractResource
{
    /**
     * @param string|null $username
     *
     * @return array
     */
    public function get($username = null)
    {
        if ($username === null) {
            $users = $this->entityManager->getRepository('App\Entity\User')->findAll();
            $users = array_map(
                function ($user) {
                    /** @var User $user */
                    return $user->getArrayCopy();
                },
                $users
            );

            return $users;
        } else {
            $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
                array('username' => $username)
            );
            if ($user) {
                return $user->getArrayCopy();
            }
        }

        return false;
    }

    /**
     * @param string|null $username
     *
     * @return User
     */
    public function getByUsername($username = null)
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
            array('username' => $username)
        );
        if ($user) {
            return $user;
        }
        return false;
    }


    /**
     * @param string $userId
     * @return User|InvalidArgumentException
     *
     */
    public function getByUserId($userId = null)
    {
        if($userId == null){
            return new InvalidArgumentException("Param 'userId' must not be null");
        }else{
            /** @var User $user */
            $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
                array('id' => $userId)
            );
            if ($user) {
                return $user;
            }else{
                throw new InvalidArgumentException("'UserId' does not exist.");
            }
        }


    }

    /**
     * @param UserDTO $userDTO
     * @return UserDTO
     *
     */
    public function createNew(UserDTO $userDTO)
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
            array('username' => $userDTO->getUsername())
        );
        if($user){
            //Username is already taken
            $userDTO->setSuccess(false);
            $userDTO->setMessage("Username already taken.");
            return $userDTO;
        }else{
            //Create a new User using the values from the UserDTO
            /** @var User $newUser */
            $newUser = new User($userDTO->getHashCode());
            $newUser->setAuthToken($userDTO->getAuthToken());
            $newUser->setEmail($userDTO->getEmail());
            $newUser->setId($userDTO->getId());
            $newUser->setPassword($userDTO->getPassword());
            $newUser->setUsername($userDTO->getUsername());

            //Persist in the Entity Manager
            $this->entityManager->persist($newUser);
            $this->entityManager->flush();

            //Check if the persistence was successful
            /** @var User $persistedUserObj */
            $persistedUserObj = $this->getByUsername($userDTO->getUsername());
            if($persistedUserObj){
                $userDTO->setSuccess(true);
                $userDTO->setMessage("Successfully saved the new User.");
                return $userDTO;
            }else{
                $userDTO->setSuccess(false);
                $userDTO->setMessage("Failed to persist the user to the database.");
                return $userDTO;
            }
        }
    }

}