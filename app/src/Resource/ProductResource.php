<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/13/2017
 * Time: 9:04 PM
 */

namespace App\Resource;

use App\AbstractResource;
use App\DTO\ProductDTO;
use App\DTO\UserDTO;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Driver\PDOException;
use ProductStatus;
use Psr\Log\InvalidArgumentException;

/**
 * Class Resource
 * @package App\Resource
 */
class ProductResource extends AbstractResource
{
    /**
     * @param string|null $username
     *
     * @param bool $getOnlyAvailable
     * @return array|InvalidArgumentException
     */
    public function get($username = null, $getOnlyAvailable = true)
    {
        if ($username === null) {
            //Return Error Response: Need username to lookup products
            return new InvalidArgumentException("Param 'username' must not be null");
        } else {
            /** @var User $user */
            $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
                array('username' => $username)
            );
            if ($user) {
                if($getOnlyAvailable){
                    $products = $this->entityManager->getRepository('App\Entity\Product')->findBy(
                        array(
                            'user' => $user->getId(),
                            'status' => ProductStatus::Available
                        )
                    );

                }else{
                    $products = $this->entityManager->getRepository('App\Entity\Product')->findBy(
                        array(
                            'user' => $user->getId(),
                            'status' => ProductStatus::Available
                        )
                    );
                }

                if($products){
                    $products = array_map(
                        function ($product) {
                            /** @var Product $product */
                            return $product->getArrayCopy();
                        },
                        $products
                    );
                    return $products;
                }else{
                    //No Products were found, so return an empty array
                    return array();
                }

            }else{
                //Return Error response: Could not find username specified
                throw new InvalidArgumentException("Param 'username' does not exist.");
            }
        }
    }

    /**
     * @param null $productId
     * @param bool $getOnlyActive
     * @return Product|InvalidArgumentException
     */
    public function getByProductId($productId = null, $getOnlyActive = true)
    {
        /** @var Product $product */
        if($getOnlyActive){
            $product = $this->entityManager->getRepository('App\Entity\Product')->findOneBy(
                array(
                    'id' => $productId,
                    'status' => ProductStatus::Available
                )
            );
        }else{
            $product = $this->entityManager->getRepository('App\Entity\Product')->findOneBy(
                array(
                    'id' => $productId
                )
            );
        }

        if ($product) {
            return $product;
        }
        //Return error response: Could not find product id specified
        throw new InvalidArgumentException("Param 'productId' does not exist.");
    }

    /**
     * @param null $hashCode
     * @param bool $getOnlyActive
     * @return Product|InvalidArgumentException
     */
    public function getByHashCode($hashCode = null, $getOnlyActive = true)
    {
        if($getOnlyActive){
            $product = $this->entityManager->getRepository('App\Entity\Product')->findOneBy(
                array(
                    'hash_code' => $hashCode,
                    'status' => ProductStatus::Available
                )
            );
        }else{
            /** @var Product $product */
            $product = $this->entityManager->getRepository('App\Entity\Product')->findOneBy(
                array('hash_code' => $hashCode)
            );
        }

        if ($product) {
            return $product;
        }
        //Return error response: Could not find product id specified
        throw new InvalidArgumentException("Param 'hash_code' does not exist.");
    }

    /**
     * @param ProductDTO $productDTO
     * @return ProductDTO
     */
    public function createNew(ProductDTO $productDTO)
    {
        //Create a new Product using the values from the ProductDTO
        /** @var Product $newProduct */
        $newProduct = new Product($productDTO->getHashCode());
        if($productDTO->getUser() != null){
            $newProduct->setUser($productDTO->getUser());
        }

        if($productDTO->getCreated() != null){
            $newProduct->setPostedDate($productDTO->getCreated());
        }

        if($productDTO->getDescription() != null){
            $newProduct->setDescription($productDTO->getDescription());
        }else{
            $newProduct->setDescription("");
        }

        if($productDTO->getImageSourcePath() != null){
            $newProduct->setImageSourcePath($productDTO->getImageSourcePath());
        }

        if($productDTO->getPrice() != null){
            $newProduct->setPrice($productDTO->getPrice());
        }

        if($productDTO->getTitle() != null){
            $newProduct->setTitle($productDTO->getTitle());
        }

        $newProduct->setStatus('available');

        //Persist in the Entity Manager
        $this->entityManager->persist($newProduct);
        $this->entityManager->flush();

        //Check if the persistence was successful
        /** @var Product $persistedProductObj */
        $persistedProductObj = $this->entityManager->getRepository('App\Entity\Product')->findOneBy(
            array('hash_code' => $productDTO->getHashCode())
        );
        if($persistedProductObj){
            $productDTO->setSuccess(true);
            $productDTO->setMessage("Successfully saved the new Product.");
            return $productDTO;
        }else{
            $productDTO->setSuccess(false);
            $productDTO->setMessage("Failed to persist the product to the database.");
            return $productDTO;
        }
    }


    /**
     * @param ProductDTO $productDTO
     * @return ProductDTO
     */
    public function updateProduct(ProductDTO $productDTO)
    {
        //Check if the persistence was successful

        /** @var Product $product */
        $product = $this->entityManager->getRepository('App\Entity\Product')->findOneBy(
            array('id' => $productDTO->getId())
        );
        if($product){
            //Create an updated Product using the values from the ProductDTO
            /** @var Product $product */
            $product->setUser($productDTO->getUser());
            $product->setStatus($productDTO->getStatus());
            $product->setImageSourcePath($productDTO->getImageSourcePath());
            $product->setDescription($productDTO->getDescription());
            $product->setTitle($productDTO->getTitle());

            //Persist in the Entity Manager
            $this->entityManager->persist($product);
            $this->entityManager->flush();

            $productDTO->setSuccess(true);
            $productDTO->setMessage("Successfully saved the updated Product.");
            return $productDTO;
        }else{
            $productDTO->setSuccess(false);
            $productDTO->setMessage("Failed to persist the product to the database.");
            return $productDTO;
        }
    }

    /**
     * @param ProductDTO $productDTO
     * @return ProductDTO
     */
    public function deleteProduct(ProductDTO $productDTO)
    {
        //Check if the persistence was successful

        /** @var Product $product */
        $product = $this->entityManager->getRepository('App\Entity\Product')->findOneBy(
            array('id' => $productDTO->getId())
        );
        if($product){

            $product->setStatus(ProductStatus::Deleted);

            //Persist in the Entity Manager
            $this->entityManager->persist($product);
            $this->entityManager->flush();

            $productDTO->setSuccess(true);
            $productDTO->setMessage("Successfully removed the Product.");
            return $productDTO;
        }else{
            $productDTO->setSuccess(false);
            $productDTO->setMessage("Failed to persist the product to the database.");
            return $productDTO;
        }
    }

}