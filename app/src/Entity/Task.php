<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/13/2017
 * Time: 9:02 PM
 */

namespace App\Entity;

use App\Entity;

use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tasks")
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $icon;

    /**
     * @ORM\Column(type="boolean", name="is_active", options={"default": true})
     */
    protected $active;

    /**
     *
     * @ORM\Column(type="datetime")
     *
     */
    protected $deadline;

    /**
     *
     * @ORM\Column(type="string", length=32)
     *
     */
    protected $priority;

    /**
     *
     * @ORM\Column(type="string", length=128)
     *
     */
    protected $title;

    /**
     *
     * @ORM\Column(type="string", length=256)
     *
     */
    protected $description;
    /**
     *
     * @ORM\Column(type="boolean")
     *
     */
    protected $completed;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $hash_code;

    /**
     * @ORM\Column(type="integer", name="task_order")
     *
     * We keep the TaskItems in sequential order, despite being saved by unordered GUID in the database
     *
     */
    protected $order;

    /**
     * @var User|null the user this task belongs (if any)
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tasks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;


    /**
     * Task constructor.
     * @param $hash_code
     *
     */
    public function __construct($hash_code){

        $this->hash_code = $hash_code;
    }


    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * Get task id
     *
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        if($id !== null){
            $this->id = $id;
        }
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }


    /**
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param integer $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }



    /**
     * @return User|null
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Sets a new user and cleans the previous one if set
     * @param User $user
     * @throws InvalidArgumentException
     */
    public function setUser($user) {
        if($user === null) {
            if($this->user !== null) {
                $this->user->getTasks()->removeElement($this);
            }
            $this->user = null;
        } else {
            if(!$user instanceof User) {
                throw new InvalidArgumentException('$group must be null or instance of HelloWorld\UserGroup');
            }
            if($this->user !== null) {
                $this->user->getTasks()->removeElement($this);
            }
            $this->user = $user;
            $user->getTasks()->add($this);
        }
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        if($icon !== null) {
            $this->icon = $icon;
        }
    }

    /**
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \DateTime $deadline
     */
    public function setDeadline($deadline)
    {
        if($deadline !== null) {
            $this->deadline = $deadline;
        }
    }

    /**
     * @return string
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param string $priority
     */
    public function setPriority($priority)
    {
        if($priority !== null) {
            $this->priority = $priority;
        }
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        if($title !== null) {
            $this->title = $title;
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        if($description !== null) {
            $this->description = $description;
        }
    }

    /**
     * @return boolean
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * @param boolean $completed
     */
    public function setCompleted($completed)
    {
        if($completed !== null) {
            $this->completed = $completed;
        }
    }

    /**
     * @return string
     */
    public function getHashCode()
    {
        return $this->hash_code;
    }

    /**
     * @param string $hash_code
     */
    public function setHashCode($hash_code)
    {
        if($hash_code !== null) {
            $this->hash_code = $hash_code;
        }
    }



}