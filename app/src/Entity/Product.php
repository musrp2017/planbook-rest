<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/13/2017
 * Time: 9:02 PM
 */

namespace App\Entity;

use App\Entity;

use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $imageSourcePath;

    /**
     * @ORM\Column(type="integer")
     *
     * See 'Enum\ProductStatus' for what this column's values correspond to
     *
     */
    protected $status;

    /**
     *
     * @ORM\Column(type="datetime")
     *
     */
    protected $postedDate;

    /**
     * @ORM\Column(type="integer")
     */
    protected $price;

    /**
     *
     * @ORM\Column(type="string", length=128)
     *
     */
    protected $title;

    /**
     *
     * @ORM\Column(type="string", length=256)
     *
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $hash_code;

    /**
     * @var User|null the user this product is made available to (if any)
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="products")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * Product constructor.
     * @param $hash_code
     *
     */
    public function __construct($hash_code){

        $this->hash_code = $hash_code;
    }

    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * Get product id
     *
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        if($id !== null){
            $this->id = $id;
        }
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return User|null
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Sets a new user and cleans the previous one if set
     * @param User $user
     * @throws InvalidArgumentException
     */
    public function setUser($user) {
        if($user === null) {
            if($this->user !== null) {
                $this->user->getProducts()->removeElement($this);
            }
            $this->user = null;
        } else {
            if(!$user instanceof User) {
                throw new InvalidArgumentException('$user must be null or instance of App\Entity\User');
            }
            if($this->user !== null) {
                $this->user->getProducts()->removeElement($this);
            }
            $this->user = $user;
            $user->getProducts()->add($this);
        }
    }


    /**
     * @return \DateTime
     */
    public function getPostedDate()
    {
        return $this->postedDate;
    }

    /**
     * @param \DateTime $postedDate
     */
    public function setPostedDate($postedDate)
    {
        if($postedDate !== null) {
            $this->postedDate = $postedDate;
        }
    }


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        if($title !== null) {
            $this->title = $title;
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        if($description !== null) {
            $this->description = $description;
        }
    }


    /**
     * @return string
     */
    public function getHashCode()
    {
        return $this->hash_code;
    }

    /**
     * @param string $hash_code
     */
    public function setHashCode($hash_code)
    {
        if($hash_code !== null) {
            $this->hash_code = $hash_code;
        }
    }

    /**
     * @return string
     */
    public function getImageSourcePath()
    {
        return $this->imageSourcePath;
    }

    /**
     * @param string $imageSourcePath
     */
    public function setImageSourcePath($imageSourcePath)
    {
        $this->imageSourcePath = $imageSourcePath;
    }

    /**
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param integer $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }





}