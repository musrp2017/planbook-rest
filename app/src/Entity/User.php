<?php
/**
 * Created by PhpStorm.
 * User: Andrew.Parise
 * Date: 10/13/2017
 * Time: 9:02 PM
 */

namespace App\Entity;

use App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=128)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $auth_token;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $hash_code;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="user")
     */
    protected $tasks;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="user")
     */
    protected $products;

    /**
     * User constructor.
     * @param $hash_code
     *
     */
    public function __construct($hash_code){
        $this->tasks = new ArrayCollection();

        $this->hash_code = $hash_code;
    }

    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * Get user id
     *
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get user username
     *
     * @ORM\return string
     */
    public function getUsername()
    {
        return $this->username;
    }


    /**
     * Get user password
     *
     * @ORM\return string
     */
    public function getPassword()
    {
        return $this->password;
    }


    /**
     * Get user email
     *
     * @ORM\return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get user auth_token
     *
     * @ORM\return string
     */
    public function getAuthToken()
    {
        return $this->auth_token;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Collection $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }



    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    /**
     * @param mixed $auth_token
     */
    public function setAuthToken($auth_token)
    {
        $this->auth_token = $auth_token;
    }

    /**
     * @return mixed
     */
    public function getHashCode()
    {
        return $this->hash_code;
    }

    /**
     * @param mixed $hash_code
     */
    public function setHashCode($hash_code)
    {
        $this->hash_code = $hash_code;
    }

    /**
     * @return Collection
     */
    public function getTasks() {
        return $this->tasks;
    }




}