<?php
// Routes

$app->get('/', 'App\Action\HomeAction:Dispatch');

$app->get('/api/photos', 'App\Action\PhotoAction:fetch');
$app->get('/api/photos/{slug}', 'App\Action\PhotoAction:fetchOne');

//User Endpoints
$app->get('/api/users', 'App\Action\UserAction:fetch');
$app->get('/api/users/{username}', 'App\Action\UserAction:fetchOne');
//!User Endpoints

//Task Endpoints
$app->get('/api/users/{user_id}/tasks', 'App\Action\TaskAction:getAllTasksByUsername');
$app->get('/api/users/{user_id}/tasks/', 'App\Action\TaskAction:getAllTasksByUsername');

$app->post('/api/users/{user_id}/tasks/new', 'App\Action\TaskAction:createNewTask');
$app->put('/api/users/{user_id}/tasks/new', 'App\Action\TaskAction:createNewTask');
$app->post('/api/users/{user_id}/tasks/{task_id}', 'App\Action\TaskAction:createNewTask');
$app->put('/api/users/{user_id}/tasks/{task_id}', 'App\Action\TaskAction:createNewTask');

$app->get('/api/users/{user_id}/tasks/{task_id}', 'App\Action\TaskAction:getTaskByTaskId');
$app->post('/api/users/{user_id}/tasks/{task_id}/edit', 'App\Action\TaskAction:updateTask');
$app->put('/api/users/{user_id}/tasks/{task_id}/edit', 'App\Action\TaskAction:updateTask');
$app->delete('/api/users/{user_id}/tasks/{task_id}/remove', 'App\Action\TaskAction:removeTask');

$app->get('/api/tasks/{task_id}', 'App\Action\TaskAction:getTaskByTaskId');
$app->post('/api/tasks/{task_id}/edit', 'App\Action\TaskAction:updateTask');
$app->put('/api/tasks/{task_id}/edit', 'App\Action\TaskAction:updateTask');
//!Task Endpoints

//Product Endpoints for the Marketplace
$app->get('/api/users/{user_id}/product', 'App\Action\ProductAction:getAllProductsByUsername');
$app->get('/api/users/{user_id}/product/', 'App\Action\ProductAction:getAllProductsByUsername');

$app->post('/api/users/{user_id}/product/new', 'App\Action\ProductAction:createNewProduct');
$app->put('/api/users/{user_id}/product/new', 'App\Action\ProductAction:createNewProduct');
$app->post('/api/users/{user_id}/product/{product_id}', 'App\Action\ProductAction:createNewProduct');
$app->put('/api/users/{user_id}/product/{product_id}', 'App\Action\ProductAction:createNewProduct');

$app->get('/api/users/{user_id}/product/{product_id}', 'App\Action\ProductAction:getProductByProductId');
$app->post('/api/users/{user_id}/product/{product_id}/edit', 'App\Action\ProductAction:updateProduct');
$app->put('/api/users/{user_id}/product/{product_id}/edit', 'App\Action\ProductAction:updateProduct');
$app->delete('/api/users/{user_id}/product/{product_id}/remove', 'App\Action\ProductAction:removeProduct');

$app->get('/api/product/{product_id}', 'App\Action\ProductAction:getProductByProductId');
$app->post('/api/product/{product_id}/edit', 'App\Action\ProductAction:updateProduct');
$app->put('/api/product/{product_id}/edit', 'App\Action\ProductAction:updateProduct');
//!Product Endpoints

//Auth Endpoints
$app->get('/api/auth', 'App\Action\AuthAction:checkAuthStatus');
$app->post('/api/auth/login', 'App\Action\AuthAction:logInUser');
$app->post('/api/auth/signup', 'App\Action\AuthAction:signUp');
$app->post('/api/auth/logout', 'App\Action\AuthAction:logoutUser');
//!Auth Endpoints

