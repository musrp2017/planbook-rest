<?php
// DIC configuration

use Slim\Container;

$container = $app->getContainer();

// -----------------------------------------------------------------------------
// Service providers
// -----------------------------------------------------------------------------

// Twig
/**
 * @param Container $c
 * @return \Slim\Views\Twig
 */
$container['view'] = function ($c) {
    $settings = $c->get('settings');
    $view = new \Slim\Views\Twig($settings['view']['template_path'], $settings['view']['twig']);

    // Add extensions
    $view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $c->get('request')->getUri()));
    $view->addExtension(new Twig_Extension_Debug());

    return $view;
};

// Flash messages
/**
 * @param Container $c
 * @return \Slim\Flash\Messages
 */
$container['flash'] = function ($c) {
    return new \Slim\Flash\Messages;
};

/**
 * @param Container $c
 * @return \Slim\Csrf\Guard
 */
$container['csrf'] = function ($c) {
    return new \Slim\Csrf\Guard;
};


///
// -----------------------------------------------------------------------------
// Service factories
// -----------------------------------------------------------------------------

// monolog
/**
 * @param Container $c
 * @return \Monolog\Logger
 */
$container['logger'] = function ($c) {
    $settings = $c->get('settings');
    $logger = new \Monolog\Logger($settings['logger']['name']);
    $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
    $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['logger']['path'], \Monolog\Logger::DEBUG));
    return $logger;
};

// Doctrine
/**
 * @param Container $c
 * @return \Doctrine\ORM\EntityManager
 */
$container['em'] = function ($c) {


    $settings = $c->get('settings');
    $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
        $settings['doctrine']['meta']['entity_path'],
        $settings['doctrine']['meta']['auto_generate_proxies'],
        $settings['doctrine']['meta']['proxy_dir'],
        $settings['doctrine']['meta']['cache'],
        false
    );
    return \Doctrine\ORM\EntityManager::create($settings['doctrine']['connection'], $config);
};


// -----------------------------------------------------------------------------
// Action factories
// -----------------------------------------------------------------------------

/**
 * @param Container $c
 * @return \App\Action\HomeAction
 */
$container['App\Action\HomeAction'] = function ($c) {
    return new App\Action\HomeAction($c->get('view'), $c->get('logger'));
};

/**
 * @param Container $c
 * @return \App\Action\PhotoAction
 */
$container['App\Action\PhotoAction'] = function ($c) {
    $photoResource = new \App\Resource\PhotoResource($c->get('em'));
    return new App\Action\PhotoAction($photoResource);
};

/**
 * @param Container $c
 * @return \App\Action\UserAction
 */
$container['App\Action\UserAction'] = function ($c) {
    $userResource = new \App\Resource\UserResource($c->get('em'));
    return new App\Action\UserAction($userResource, $c->get('logger'));
};

/**
 * @param Container $c
 * @return \App\Action\AuthAction
 */
$container['App\Action\AuthAction'] = function ($c) {
    $userResource = new \App\Resource\UserResource($c->get('em'));
    return new App\Action\AuthAction($userResource, $c->get('logger'));
};

/**
 * @param Container $c
 * @return \App\Action\TaskAction
 */
$container['App\Action\TaskAction'] = function ($c) {
    $userResource = new \App\Resource\UserResource($c->get('em'));
    $taskResource = new \App\Resource\TaskResource($c->get('em'));
    return new App\Action\TaskAction($userResource, $taskResource, $c->get('logger'));
};

/**
 * @param Container $c
 * @return \App\Action\ProductAction
 */
$container['App\Action\ProductAction'] = function ($c) {
    $userResource = new \App\Resource\UserResource($c->get('em'));
    $productResource = new \App\Resource\ProductResource($c->get('em'));
    return new App\Action\ProductAction($userResource, $productResource, $c->get('logger'));
};
