/**
 * @desc        backbone router for pushState page routing
 */

define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "models/SessionModel",
    "models/UserModel",
    "models/TaskItemCollection",

    "views/HeaderView",
    "views/FooterView",
    "views/IndexView",
    "views/TaskAppView",
    "views/LoginView",
    "views/RegisterView",
    "views/TermsView",
    "views/PrivacyView",
    "views/ForgotPasswordView",
    "views/SettingsView",
    "views/UserView",
    "views/PublicView",

    "common",

    "utils"
], function(
        app, _, Backbone, $,
        SessionModel, UserModel, TaskItemCollection,
        HeaderView, FooterView, IndexView, TaskAppView, LoginView, RegisterView, TermsView, PrivacyView, ForgotPasswordView, SettingsView, UserView, PublicView,
        Common
    ){

    var WebRouter = Backbone.Router.extend({

        initialize: function(){
            _.bindAll(this);
        },

        routes: {
            ""               : "index",
            "index"          : "index",

            "login"          : "public",
            "register"       : "public",
            "toc"            : "termsConditions",
            "privacyPolicy"  : "privacyPolicy",
            "recoverAccount" : "recoverAccount",

            "home"           : "userHome",
            "taskApp"        : "taskApp",
            "settings"       : "settings"

        },

        show: function(view, options){
            // Every page view in the router should need a header.
            // Instead of creating a base parent view, just assign the view to this
            // so we can create it if it doesn't yet exist
            var that = this;

            //Header
            // if(options && options.hideHeader === true){
            //     if(that.headerView){
            //         that.headerView.close();
            //     }
            // }else{
            //     if(!that.headerView){
            //         that.headerView = new HeaderView({});
            //         that.headerView.setElement( $(".header") ).render();
            //     }
            // }

            //Footer
            // if(options && options.hideFooter === true){
            //     if(that.footerView){
            //         that.footerView.close();
            //     }
            // }else{
            //     if(!that.footerView){
            //         that.footerView = new FooterView({});
            //         that.footerView.setElement( $("#footer-hook") ).render();
            //
            //     }
            // }


            // Close and unbind any existing page view
            if(this.currentView) this.currentView.close();

            // Establish the requested view into scope
            this.currentView = view;

            // Need to be authenticated before rendering view.
            // For cases like a user's settings page where we need to double check against the server.
            if (typeof options !== 'undefined' && options.requiresAuth){
                var self = this;
                app.session.checkAuth({
                    success: function(res){
                        // If auth successful, render inside the page wrapper
                        $('#content').html( self.currentView.render().$el);
                    }, error: function(res){
                        self.navigate("/", { trigger: true, replace: true });
                    }
                });

            } else {
                // Render inside the page wrapper
                $('#content').html(this.currentView.render().$el);
                //this.currentView.delegateEvents(this.currentView.events);        // Re-delegate events (unbound when closed)
            }

        },

        index: function() {
            var that = this;
            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                if(app.session.get('logged_in')) {
                    that.navigate('home', {trigger: true, replace: true });
                } else {
                    that.navigate('register', {trigger: true, replace: true });
                }
            }
        },

        settings: function() {
            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                this.show( new SettingsView({}) );
            }
        },

        taskApp: function() {

            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                this.show( new TaskAppView({}) );
            }

        },

        userHome: function() {

            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                this.show( new UserView({}) );
            }

        },

        login: function() {

            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                this.show( new LoginView({}) );
            }

        },

        recoverAccount: function() {

            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                this.show( new ForgotPasswordView({}) );
            }

        },

        register: function() {

            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                this.show( new RegisterView({}));
            }

        },

        public: function() {

            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                this.show( new PublicView({}), {hideHeader: true, hideFooter: true} );
            }

        },

        privacyPolicy: function() {

            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                this.show( new PrivacyView({}) );
            }

        },

        termsConditions: function() {

            // Fix for non-pushState routing (IE9 and below)
            var hasPushState = !!(window.history && history.pushState);
            if(!hasPushState) this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});
            else {
                this.show( new TermsView({}) );
            }

        }

    });

    return WebRouter;
});