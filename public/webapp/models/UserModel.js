/**
 * @desc		stores the POST state and response state of authentication for user
 */
define([
    "app",
    "underscore",
    "backbone"
], function(
    app, _, Backbone
){

    var UserModel = Backbone.Model.extend({

        initialize: function(){
            _.bindAll(this);
        },

        defaults: {
            id      : -1,
            username: '',
            password: '',
            email   : '',
            fullName: 'Anonymous',
            profilePicture: ''
        },

        url: function(){
            return app.API + '/users/' + this.id;
        }

    });

    return UserModel;
});