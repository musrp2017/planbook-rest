/**
 * @desc		stores the POST state and response state of one market item for the user
 */
define([
    "app",

    "underscore",
    "backbone"
], function(
    app,
    _, Backbone
){

    var MarketItemModel = Backbone.Model.extend({

        initialize: function(){
            _.bindAll(this);
        },

        defaults: {
            id: -1,
            imageSourcePath: '',
            price: '',
            title: '',
            description: '',
            status: '',
            hash_code: '',
            order: -1
        },

        url: function(){
            return app.API + '/users/' + app.session.user.id + "/product" ;
        },

        parse: function(response){
            var that = this;

            //check if this is a new model or not
            if (response.isNewModel) {
                // Do not parse a second time
                // Do the parsing logic.
                var marketItem = response.marketItem;
                if(marketItem){
                    return response.marketItem;
                }
            }

            return response
        },

        "sync": function(method, model, options){
            var that = this;

            if(model.attributes.isNewObj){
                //this attribute is added in the defaults for a new obj
                options.url = that.url() + '/new';
            }else if(method==='GET'){
                //this method is only present when fetch is called for this item
                options.url = that.url() + "/" + that.id;
            }else if(method==='delete'){
                //if the user presses the red "X" to remove from the dashboard
                options.url = that.url() + "/" + that.id + "/remove";
            }else{
                options.url = that.url() + "/" + that.id + "/edit";
            }

            return Backbone.sync(method, model, options);
        },

        // Toggle the `available` state of this task item.
        toggle: function () {
            var curStatus = this.get('status');
            if(curStatus === 'available'){
                this.save({
                    status: 'purchased'
                });
            }
        }

    });

    return MarketItemModel;
});