define([
    'app',
    "jquery",
    'underscore',
    'backbone',

    'models/MarketItemModel'
], function (
    app, $, _, Backbone,
    MarketItemModel
) {

    var MarketItemCollection = Backbone.Collection.extend({
        // Reference to this collection's model.
        model: MarketItemModel,

        url: function(){
            return app.API + '/users/' + app.session.user.id + "/product";
        },

        // Filter down the list of all task items that are finished.
        completed: function () {
            return this.where({completed: true});
        },

        parse: function(response){

            //values
            var marketItems = response.marketItems;
            if(marketItems){
                //verify that there is a length property

                //Parse the response and construct models
                for (var i = 0, length = marketItems.length; i < length; i++) {

                    var currentValues = marketItems[i];

                    if(currentValues){
                        var marketItemObject = MarketItemModel;
                        marketItemObject.id = currentValues.id;
                        marketItemObject.imageSourcePath = currentValues.imageSourcePath;
                        marketItemObject.title = currentValues.title;
                        marketItemObject.description = currentValues.description;
                        marketItemObject.completed = currentValues.completed;
                        marketItemObject.hash_code = currentValues.hash_code;
                        marketItemObject.order = currentValues.order;
                        marketItemObject.price = currentValues.price;
                        //push the model object
                        this.push(marketItemObject);
                    }else{
                        console.log("Error: marketItem did not have an id.")
                    }
                }
            }


            //return models
            return this.models;
        },

        // Filter down the list to only task items that are still not finished.
        remaining: function () {
            return this.where({completed: false});
        },

        // We keep the TaskItems in sequential order, despite being saved by unordered
        // GUID in the database. This generates the next order number for new items.
        nextOrder: function () {
            return this.length ? this.last().get('order') + 1 : 1;
        },

        // task items are sorted by their original insertion order.
        comparator: 'order'
    });

    return new MarketItemCollection();
});