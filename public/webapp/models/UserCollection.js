/**
 * @desc		stores the POST state and response state of authentication for user
 */
define([
    "app",
    "models/UserModel",
    "userscore",
    "backbone"
], function(app, UserModel, _, Backbone){

    var UserCollection = Backbone.Model.extend({

        initialize: function(){
            _.bindAll(this);
        },

        model:UserModel,
        url: function(){
            return app.API + '/users';
        }

    });

    return UserCollection;
});