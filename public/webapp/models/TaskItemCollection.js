define([
    'app',
    "jquery",
    'underscore',
    'backbone',
    'models/TaskItemModel'
], function (app, $, _, Backbone, TaskItemModel) {

    var TaskItemCollection = Backbone.Collection.extend({
        // Reference to this collection's model.
        model: TaskItemModel,

        url: function(){
            return app.API + '/users/' + app.session.user.id + "/tasks";
        },

        // Filter down the list of all task items that are finished.
        completed: function () {
            return this.where({completed: true});
        },

        parse: function(response){

            //values
            var tasks = response.tasks;
            if(tasks){
                //verify that there is a length property

                //Parse the response and construct models
                for (var i = 0, length = tasks.length; i < length; i++) {

                    var currentValues = tasks[i];

                    if(currentValues){
                        var taskObject = TaskItemModel;
                        taskObject.id = currentValues.id;
                        taskObject.icon = currentValues.icon;
                        taskObject.deadline = {};
                        taskObject.deadline.date = currentValues.deadline.date;
                        taskObject.deadline.timezone_type = currentValues.deadline.timezone_type;
                        taskObject.deadline.timezone = currentValues.deadline.timezone;
                        taskObject.priority = currentValues.priority;
                        taskObject.title = currentValues.title;
                        taskObject.description = currentValues.description;
                        taskObject.completed = currentValues.completed;
                        taskObject.hash_code = currentValues.hash_code;
                        taskObject.order = currentValues.order;
                        //push the model object
                        this.push(taskObject);
                    }else{
                        console.log("Error: task did not have an id.")
                    }
                }
            }


            //return models
            return this.models;
        },

        // Filter down the list to only task items that are still not finished.
        remaining: function () {
            return this.where({completed: false});
        },

        // We keep the TaskItems in sequential order, despite being saved by unordered
        // GUID in the database. This generates the next order number for new items.
        nextOrder: function () {
            return this.length ? this.last().get('order') + 1 : 1;
        },

        // task items are sorted by their original insertion order.
        comparator: 'order'
    });

    return new TaskItemCollection();
});