define([
    'app',
    "jquery",
    'underscore',
    'backbone',

    'models/MenuItemModel'
], function (
    app, $, _, Backbone,
    MenuItemModel
) {

    var MenuItemCollection = Backbone.Collection.extend({
        // Reference to this collection's model.
        model: MenuItemModel,

        initialize: function() {
            this.on('selected', this.onSelectedChanged, this);
        },

        onSelectedChanged: function(selectedModel) {

            //iterate over each model, if the curModel is selected, and is not the model that is selected
            this.each(function(model) {
                if (model.get('isSelected') === true && selectedModel.get('id') !== model.get('id')) {
                    model.set({isSelected: false});
                }
            });

            selectedModel.set({isSelected: true});
            this.selected = selectedModel;
            this.trigger('onSelectedItemChange', {model: selectedModel});
        }
    });

    return MenuItemCollection;
});