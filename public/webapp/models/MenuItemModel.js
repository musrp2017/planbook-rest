/**
 * @desc		stores the state for a menu item
 */
define([
    "app",

    "underscore",
    "backbone"
], function(
    app,
    _, Backbone
){

    var MenuItemModel = Backbone.Model.extend({

        initialize: function(){
            _.bindAll(this);
        },

        defaults: {
            title       : 'Default Title',
            icon        : 'icon',
            id          : 'default-id',
            tagName     : 'li',
            className   : '',
            isSelected  : false
        }

    });

    return MenuItemModel;
});