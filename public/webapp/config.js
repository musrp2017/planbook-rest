/**
 * @desc        configure aliases and dependencies
 */

if (typeof DEBUG === 'undefined') DEBUG = true;

require.config({

    baseUrl: '/planbook-rest/public/webapp',

    paths: {
        'jquery'                : './node_modules/jquery/dist/jquery',
        'underscore'            : './node_modules/underscore/underscore',         // load lodash instead of underscore (faster + bugfixes)
        'backbone'              : './node_modules/backbone/backbone',
        'bootstrap'             : './node_modules/bootstrap/dist/js/bootstrap',
        'text'                  : './node_modules/text/text',
        'parsley'               : './lib/parsley/parsley',
        'jquery.lettering'      : './lib/jquery.lettering',
        'TweenMax'              : './lib/TweenMax.min'
    },

    // non-AMD lib
    shim: {
        'underscore'            : { exports  : '_' },
        'backbone'              : { deps : ['underscore', 'jquery'], exports : 'Backbone' },
        'bootstrap'             : { deps : ['jquery'], exports : 'Bootstrap' },
        'parsley'               : { deps: ['jquery'] },
        'jquery.lettering'      : { deps: ['jquery'] }
    }

});

require(['main']);           // Initialize the application with the main application file.