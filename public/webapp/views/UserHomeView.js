define([
    "app",
    "backbone",
    "underscore",

    "text!templates/UserHomeViewTemplate.html",
    "text!templates/UnderConstructionViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, Backbone, _,
    UserHomeTpl, UnderConstructionViewTpl
){

    var UserHomeView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.render);
        },

        events: {

        },


        render:function () {
            var that = this;
            if(app.session.get('logged_in')){
                // that.template = _.template(UserHomeTpl); //When this page is made, replace it with below
                that.template = _.template(UnderConstructionViewTpl);

                that.$el.html(this.template({
                    logged_in: app.session.get("logged_in"),
                    user: app.session.user.toJSON()
                }));
            } else {
                app.router.navigate('home', { trigger : true, replace : true });
                that.close();
            }

            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return UserHomeView;
});