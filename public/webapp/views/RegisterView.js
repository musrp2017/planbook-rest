define([
    "app",
    "backbone",
    "underscore",

    "text!templates/RegisterViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, Backbone, _,
    RegisterTpl
){

    var RegisterView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.render);
        },

        events: {
            'click #signup-btn'           : 'onSignupAttempt',
            'click #login-page-link'      : 'onLoginLinkPress',
            'keyup #signupPasswordagain'  : 'onConfirmPasswordKeyup',
            'keyup #signupEmailagain'     : 'onConfirmEmailKeyup'
        },

        // Allow enter press to trigger signup
        onConfirmPasswordKeyup: function(evt){
            var k = evt.keyCode || evt.which;

            if (k == 13 && $('#signupPassword').val() === ''){
                evt.preventDefault();   // prevent enter-press submit when input is empty
            } else if(k == 13){
                evt.preventDefault();
                this.onSignupAttempt();
                return false;
            }
        },
        // Allow enter press to trigger signup
        onConfirmEmailKeyup: function(evt){
            var k = evt.keyCode || evt.which;

            if (k == 13 && $('#signupEmail').val() === ''){
                evt.preventDefault();   // prevent enter-press submit when input is empty
            } else if(k == 13){
                evt.preventDefault();
                this.onSignupAttempt();
                return false;
            }
        },

        onSignupAttempt: function(evt){
            if(evt) evt.preventDefault();
            if(this.$(".register-form").parsley('validate')){
                app.session.signup({
                    username: this.$("#signupUsername").val(),
                    password: this.$("#signupPassword").val(),
                    email:    this.$("#signupEmail").val()
                }, {
                    success: function(mod, res){
                        if(DEBUG) console.log("SUCCESS", mod, res);

                    },
                    error: function(err){
                        if(DEBUG) console.log("ERROR", err);
                        app.showAlert('System failed to save data.', err.error, 'alert-danger');
                    }
                });
            } else {
                // Invalid clientside validations thru parsley
                if(DEBUG) console.log("Did not pass client side validation");

            }
        },

        onLoginLinkPress: function(evt){
            if(evt) evt.preventDefault();
            app.router.navigate('login', { trigger : true, replace : true });
        },

        render:function () {
            if(app.session.get('logged_in')){
                app.router.navigate('home', { trigger : true, replace : true });
                this.close();
            } else {
                this.template = _.template(RegisterTpl);
                this.$el.html(this.template({
                    logged_in: app.session.get("logged_in"),
                    user: app.session.user.toJSON()
                }));
            }

            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return RegisterView;
});