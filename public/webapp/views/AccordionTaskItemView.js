define([
    "app",
    "jquery",
    "underscore",
    "backbone",

    "text!templates/AccordionTaskItemViewTemplate.html",

    "common",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, $, _, Backbone,
    TaskItemTpl,
    Common
){

    var AccordionTaskItemView = Backbone.View.extend({
        tagName: 'li',
        className:'task-item',

        initialize: function () {
            _.bindAll(this);

            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'update', this.render);
            this.listenTo(this.model, 'destroy', this.remove);
            this.listenTo(this.model, 'visible', this.toggleVisible);


            this.completeBoxId = "checkBtn-" + this.model.get("hash_code");
            this.completeTextId = "completeText-" + this.model.get("hash_code");

        },

        events: {
            'click .checkbox-label' :	    'toggleCompleted',
            'dblclick label'        :	    'edit',
            'click .destroy'        :	    'clear',
            'keypress .edit'        :	    'updateOnEnter',
            'keydown .edit'         :	    'revertOnEscape',
            'blur .edit'            :		'closeEditingMode'
        },


        render:function () {

            this.template = _.template(TaskItemTpl);

            this.$el.html(this.template(this.model.toJSON()));

            this.$el.toggleClass('completed', this.model.get('completed'));

            this.toggleVisible();
            this.$input = this.$('.edit');

            return this;
        },

        toggleVisible: function () {
            this.$el.toggleClass('hidden',  this.isHidden());
        },

        isHidden: function () {
            var isCompleted = this.model.get('completed');
            return (// hidden cases only
                (!isCompleted && Common.TaskFilter === 'completed') ||
                (isCompleted && Common.TaskFilter === 'active')
            );
        },

        // Toggle the `"completed"` state of the model.
        toggleCompleted: function () {
            this.model.toggle();
        },

        // Switch this view into `"editing"` mode, displaying the input field.
        edit: function () {
            this.$el.addClass('editing');
            this.$input.focus();
        },

        // Close the `"editing"` mode, saving changes to the task.
        closeEditingMode: function () {
            var value = this.$input.val();
            var trimmedValue = value.trim();

            if (trimmedValue) {
                this.model.save({ title: trimmedValue });

                if (value !== trimmedValue) {
                    // Model values changes consisting of whitespaces only are not causing change to be triggered
                    // Therefore we've to compare untrimmed version with a trimmed one to chech whether anything changed
                    // And if yes, we've to trigger change event ourselves
                    this.model.trigger('change');
                }
            } else {
                this.clear();
            }

            this.$el.removeClass('editing');
        },

        // If you hit `enter`, we're through editing the item.
        updateOnEnter: function (e) {
            if (e.keyCode === Common.ENTER_KEY) {
                this.closeEditingMode();
            }
        },

        // If you're pressing `escape` we revert your change by simply leaving
        // the `editing` state.
        revertOnEscape: function (e) {
            if (e.which === Common.ESCAPE_KEY) {
                this.$el.removeClass('editing');
                // Also reset the hidden input back to the original value.
                this.$input.val(this.model.get('title'));
            }
        },

        clear: function () {
            this.model.destroy();
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return AccordionTaskItemView;
});