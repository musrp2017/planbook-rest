define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "models/TaskItemCollection",
    "views/TaskItemView",

    "text!templates/TaskStatisticsViewTemplate.html",
    "text!templates/TaskListViewTemplate.html",

    "common",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    TaskItemCollection, TaskItemView,
    TaskStatsViewTpl, TaskListViewTpl,
    Common
){

    var TaskListView = Backbone.View.extend({

        itemView: TaskItemView,
        collection: TaskItemCollection,
        items: [],

        initialize: function () {
            _.bindAll(this, 'addSingleItem');

            this.collection.fetch(
                {
                    reset:true,
                    success: function(collection){
                        // This code block will be triggered only after receiving the data.
                        console.log(collection.toJSON());
                    }
                }
            );

            this.setupListeners();

        },

        events: {
            'click #clear-completed'        :   'clearCompleted',
            'keypress #new-task'            :   'createOnEnter',
            'click #toggle-all'             :   'toggleAllComplete',
            'click #btnStatsFilterCompleted':   'filterAllShowCompleted',
            'click #btnStatsFilterActive'   :   'filterAllShowActive'

        },


        setupListeners: function() {
            this.listenTo(this.collection, 'add', this.addSingleItem, this);
            this.listenTo(this.collection, 'reset', this.addAll, this);
            this.listenTo(this.collection, 'remove', this.removeSingleItem, this);
            this.listenTo(this.collection, 'change:completed', this.filterShowVisible, this);
            this.listenTo(this.collection, 'filter', this.filterAll, this);
            this.listenTo(this.collection, 'change', this.renderStats, this);
        },

        toggleAllComplete: function () {
            var that = this;
            this.allCheckbox = this.$('#toggle-all')[0];

            this.completed = this.allCheckbox.checked;

            this.collection.each(function (taskItem) {
                taskItem.save({
                    completed: that.completed
                });
            });
        },

        // If you hit return in the main input field, create new **TaskItem** model,
        createOnEnter: function (e) {
            this.input =  this.$('#new-task');
            if (e.which !== Common.ENTER_KEY || !this.input.val().trim()) {
                return;
            }

            this.collection.create(this.newAttributes());
            this.input.val('');
        },

        // Generate the default attributes for a new task item.
        newAttributes: function () {
            var that = this;
            this.input =  this.$('#new-task');
            return {
                title: that.input.val().trim(),
                order: that.collection.nextOrder(),
                completed: false,
                isNewObj: true
            };
        },

        render:function () {
            var that = this;

            this.template = _.template(TaskListViewTpl);
            this.main = this.$('#main');

            this.$el.html(that.template({
                user: app.session.user.toJSON()
            }));

            this.main.show();


            this.addAll();


            return this;
        },


        renderStats:function(){
            var that = this;

            this.allCheckbox = this.$('#toggle-all')[0];

            this.completed = this.collection.completed().length;
            this.remaining = this.collection.remaining().length;

            this.statsTemplate = _.template(TaskStatsViewTpl);
            this.footer = that.$('#footer');

            if (this.collection.length) {
                that.footer.show();

                that.footer.html(that.statsTemplate({
                    completed: that.completed,
                    remaining: that.remaining
                }));

                that.$('#filters li a')
                    .removeClass('selected')
                    .filter('[href="#/' + (Common.TaskFilter || '') + '"]')
                    .addClass('selected');

                that.allCheckbox.checked = !that.remaining;
            }
        },

        addSingleItem: function(model) {
            if(this.getViewByModel(model)){
                //Check if the model was already added
                return;
            }
            var viewItem = new this.itemView({
                model: model
            });
            this.items.push(viewItem);
            this.addListItemListeners(viewItem);

            viewItem.render();
            this.$('#task-list').append(viewItem.el);


            return viewItem;
        },

        //propagate list item events through parent list view
        //propagates the single listview and any additional parameters to the listview
        addListItemListeners: function(view) {
            // this.listenTo(view, 'all', function() {
            //     var eventName = 'item:' + arguments[0];
            //     var params = _.toArray(arguments);
            //     params.splice(0,1);
            //     params.unshift(eventName, view);
            //     this.trigger.apply(this, params);
            // });

            // this.listenTo(view.model, 'change', function() {
            //     view.render();
            // });
        },

        removeSingleItem: function(model) {
            var view = this.getViewByModel(model);
            this.removeSingleView(view);
        },

        removeSingleView: function(view) {
            var index;
            //remove listeners
            this.stopListening(view);

            if (view) {
                this.stopListening(view.model);
                view.remove();
                index = this.items.indexOf(view);
                //remove view from items
                this.items.splice(index, 1);
            }
        },

        getViewByModel: function(model) {
            return _.find(this.items, function(item, index) {
                return item.model===model;
            });
        },

        removeAllItems: function() {
            this.collection.each(function(model) {
                this.removeSingleItem(model);
            }, this);
        },

        remove: function() {
            //do the default Backbone remove logic
            Backbone.View.prototype.remove.call(this, arguments);
            //extra remove our items - one by one
            this.removeAllItems();
        },

        // Add all items in the **TaskItemCollection** collection at once.
        addAll: function () {
            //remove previous items if present
            this.removeAllItems();
            //reinit all listeners
            this.setupListeners();
            //add new items
            this.collection.each(function(model) {
                this.addSingleItem(model);
            }, this);
        },

        filterShowCompleted: function(taskItem){
            Common.TaskFilter = 'complete' || '';
            taskItem.trigger('complete');
        },

        filterShowActive: function(taskItem){
            Common.TaskFilter = 'active' || '';
            taskItem.trigger('active');
        },

        filterShowVisible: function (taskItem) {
            Common.TaskFilter = 'visible' || '';
            taskItem.trigger('visible');
        },

        filterAllShowActive: function(){
            this.collection.each(this.filterShowActive, this);

        },

        filterAllShowCompleted: function(){
            this.collection.each(this.filterShowCompleted, this);
        },

        filterAll: function () {
            this.collection.each(this.filterShowVisible, this);
        },

        // Clear all completed task items, destroying their models.
        clearCompleted: function () {
            _.invoke(this.collection.completed(), 'destroy');
            return false;
        },

        close: function(){
            this.removeAllItems();
            this.remove();
            this.unbind();
        }

    });

    return TaskListView;
});