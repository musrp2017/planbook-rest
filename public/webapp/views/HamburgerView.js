define([
    "app",
    "jquery",
    "underscore",
    "backbone",

    "text!templates/HamburgerViewTemplate.html",

    "common",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, $, _, Backbone,
    HamburgerViewTpl,
    Common
){

    var HamburgerView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

        },

        events: {

        },


        render:function () {

            this.template = _.template(HamburgerViewTpl);

            this.$el.html(this.template({}));

            return this;
        },



        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return HamburgerView;
});