define([
    "app",
    "jquery",
    "underscore",
    "backbone",

    "text!templates/MarketItemViewTemplate.html",

    "common",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, $, _, Backbone,
    MarketItemTpl,
    Common
){

    var MarketItemView = Backbone.View.extend({
        tagName: 'li',
        id: 'market-item',

        initialize: function () {
            _.bindAll(this);

            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'update', this.render);
            this.listenTo(this.model, 'destroy', this.remove);
            this.listenTo(this.model, 'visible', this.toggleVisible);

        },

        events: {
            'click .toggle'     :	    'toggleCompleted',
            'click .destroy'    :	    'clear'
        },


        render:function () {

            this.template = _.template(MarketItemTpl);

            this.$el.html(this.template(this.model.toJSON()));

            this.$el.toggleClass('completed', this.model.get('completed'));

            this.toggleVisible();

            return this;
        },

        toggleVisible: function () {
            this.$el.toggleClass('hidden',  this.isHidden());
        },

        isHidden: function () {
            var isCompleted = this.model.get('completed');
            return (// hidden cases only
                (!isCompleted && Common.TaskFilter === 'completed') ||
                (isCompleted && Common.TaskFilter === 'active')
            );
        },

        // Toggle the `"completed"` state of the model.
        toggleCompleted: function () {
            this.model.toggle();
        },

        clear: function () {
            this.model.destroy();
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return MarketItemView;
});