define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "text!templates/AuthSwitchCardViewTemplate.html",

    'views/LoginSwitchCardView',
    'views/RegisterSwitchCardView',

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    AuthSwitchCardViewTemplate,
    LoginView, RegisterView
){

    var AuthenticationSwitchCardView = Backbone.View.extend({
        tagName: 'div',
        className: 'form-collection',

        initialize: function () {
            _.bindAll(this);

            app.session.on("change:logged_in", this.onLoginStatusChange);

        },

        events: {
            'click .below button': 'onClickBelow'
        },

        onClickBelow:function() {
            var belowCard = $('.below');
            var aboveCard = $('.above');
            var parent = $('.form-collection');

            parent.addClass('animation-state-1');

            setTimeout(function () {
                belowCard.removeClass('below');
                aboveCard.removeClass('above');
                belowCard.addClass('above');
                aboveCard.addClass('below');
                setTimeout(function () {
                    parent.addClass('animation-state-finish');
                    parent.removeClass('animation-state-1');
                    setTimeout(function () {
                        aboveCard.addClass('turned');
                        belowCard.removeClass('turned');
                        parent.removeClass('animation-state-finish');

                        if (belowCard.hasClass('sign-up-card')) {
                            $('#signup-btn').removeClass('disabled');
                            $('#login-btn').addClass('disabled');
                        }else {
                            $('#signup-btn').addClass('disabled');
                            $('#login-btn').removeClass('disabled');
                        }
                    }, 300)
                }, 10)
            }, 300);


        },

        render:function () {
            this.template = _.template(AuthSwitchCardViewTemplate);

            this.$el.html(this.template({
                user : app.session.user.toJSON()
            }));

            this.afterRender();
            this.initListeners();
            return this;
        },

        afterRender: function(){
            //Setup Login Card
            this.loginView = new LoginView();
            this.loginView.render();
            this.$('#switch-card-login-hook').append(this.loginView.el);

            //Setup Register Card
            this.registerView = new RegisterView();
            this.registerView.render();
            this.$('#switch-card-register-hook').append(this.registerView.el);
        },

        initListeners:function(){
            this.listenTo(this.loginView,       'onLoginAttempt',       this.onLoginAttempt);
            this.listenTo(this.registerView,    'onRegisterAttempt',    this.onRegisterAttempt);

        },

        onLoginAttempt:function(){
            if($('#login-btn').hasClass('disabled')) {
                //do nothing
            }else{
                //if the log in card is on top
                app.session.login({
                    username: this.$("#login-username-input").val(),
                    password: this.$("#login-password-input").val()
                }, {
                    success: function (mod, res) {
                        if (DEBUG) console.log("SUCCESS", mod, res);

                    },
                    error: function (err) {
                        if (DEBUG) console.log("ERROR", err);
                        var errMsg = '';
                        if (typeof err !== 'undefined') {
                            errMsg = err.error;
                        }
                        app.showAlert('The username you’ve entered doesn’t match any account.', errMsg, 'alert-danger');
                    }
                });
            }
        },

        onRegisterAttempt:function(){
            if($('#signup-btn').hasClass('disabled')) {
                //do nothing
            }else{
                app.session.signup({
                    username: this.$("#signupUsername").val(),
                    password: this.$("#signupPassword").val(),
                    email: this.$("#signupEmail").val()
                }, {
                    success: function (mod, res) {
                        if (DEBUG) console.log("SUCCESS", mod, res);

                    },
                    error: function (err) {
                        if (DEBUG) console.log("ERROR", err);
                        app.showAlert('System failed to save data.', err.error, 'alert-danger');
                    }
                });
            }
        },

        onLoginStatusChange: function(evt) {
            var that = this;
            if (app.session.get("logged_in")) {
                that.trigger('onLoginStatusChange');
                that.close();
            } else {
                that.render();
            }

        },

        close: function(){
            var that = this;

            if(this.loginView){
                that.loginView.close();
            }

            if(this.registerView){
                that.registerView.close();
            }

            this.remove();
            this.unbind();
        }

    });

    return AuthenticationSwitchCardView;
});