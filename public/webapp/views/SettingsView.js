define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "text!templates/SettingsViewTemplate.html",
    "text!templates/UnderConstructionViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    SettingsViewTpl, UnderConstructionViewTpl
){

    var SettingsView = Backbone.View.extend({


        initialize: function () {
            _.bindAll(this);

            app.session.on("change:logged_in", this.onLoginStatusChange);

        },

        events: {
            'click #profile-tab-header'     : 'onProfileTabClick',
            'click #password-tab-header'   : 'onPasswordTabClick'
        },

        onProfileTabClick:function(){
            this.formShow($('#profile-tab-header'), true);
            this.formShow($('#profile-tab-content'), false);

            this.formHide($('#password-tab-header'), true);
            this.formHide($('#password-tab-content'), false);
        },

        onPasswordTabClick:function(){
            this.formHide($('#profile-tab-header'), true);
            this.formHide($('#profile-tab-content'), false);

            this.formShow($('#password-tab-header'), true);
            this.formShow($('#password-tab-content'), false);

        },

        formHide:function(jqueryElem, isHeader){
            if(isHeader){
                jqueryElem.removeClass('active');
            }else{
                jqueryElem.removeClass('active in');
            }
        },

        formShow:function(jqueryElem, isHeader){
            if(isHeader){
                jqueryElem.addClass('active');

            }else{
                jqueryElem.addClass('active in');
            }
        },

        render:function () {
            // this.template = _.template(SettingsViewTpl); //When Settings page is made, replace this with below
            this.template = _.template(UnderConstructionViewTpl);

            this.$el.html(this.template({
                user : app.session.user.toJSON()
            }));

            this.afterRender();
            return this;
        },

        afterRender: function(){

        },

        onLoginStatusChange: function(evt){
            var that = this;
            if(app.session.get("logged_in")){
                that.render();
            }else{
                that.close();
            }
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return SettingsView;
});