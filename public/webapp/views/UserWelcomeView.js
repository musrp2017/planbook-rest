define([
    "app",
    "backbone",
    "underscore",
    "jquery",

    "text!templates/UserWelcomeViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap",
    "TweenMax"

], function(
    app, Backbone, _, $,
    UserWelcomeTpl
){

    var UserWelcomeView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.render);
        },

        events: {

        },


        render:function () {
            var that = this;
            if(app.session.get('logged_in')){
                that.template = _.template(UserWelcomeTpl);
                that.$el.html(this.template({
                    logged_in: app.session.get("logged_in"),
                    user: app.session.user.toJSON()
                }));
            } else {
                app.router.navigate('home', { trigger : true, replace : true });
                that.close();
            }

            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return UserWelcomeView;
});