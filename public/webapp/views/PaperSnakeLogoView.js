define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "text!templates/PaperSnakeLogoViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap",
    "jquery.lettering"
], function(
    app, _, Backbone, $,
    LogoViewTpl
){

    var PaperSnakeLogoView = Backbone.View.extend({
        tagName: 'div',

        initialize: function () {
            _.bindAll(this);

        },

        events: {

        },


        render:function () {
            this.template = _.template(LogoViewTpl);
            this.$el.html(this.template({}));

            this.afterRender();
            return this;
        },

        afterRender:function(){

        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return PaperSnakeLogoView;
});