define([
    "app",
    "backbone",
    "underscore",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, Backbone, _
){

    var IndexView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.render);
        },

        events: {

        },

        render:function () {


            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return IndexView;
});