define([
    "app",
    "backbone",
    "jquery",
    "underscore",


    "views/MenuItemView",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, Backbone, $, _,
    MenuItemView
){

    var MenuView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

        },

        events: {
            'click' : 'highlight'

        },

        render: function() {
            this.collection.each(function(model) {
                var item = new MenuItemView({model: model});
                this.$el.append(item.render().el);
            }, this);


            this.initListeners();

            return this;
        },

        initListeners:function(){
            this.listenTo(this.collection,  'onSelectedItemChange', this.onSelectedChange);

        },

        onSelectedChange:function(evt){
            this.trigger('onMenuSelectedChange', {requestedPage: evt.model.get("title")});

        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return MenuView;
});