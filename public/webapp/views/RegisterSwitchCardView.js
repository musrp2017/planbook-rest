define([
    "app",
    "backbone",
    "underscore",

    "text!templates/RegisterSwitchCardViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, Backbone, _,
    RegisterTpl
){

    var RegisterSwitchCardView = Backbone.View.extend({
        tagName: 'div',
        className: 'card elevation-2 limit-width sign-up-card above',


        initialize: function () {
            _.bindAll(this);



            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.render);
        },

        events: {
            'click #signup-btn'     : 'onSignupAttempt'

        },


        onSignupAttempt: function(evt){
            if(evt) evt.preventDefault();
            if($('#signup-btn').hasClass('disabled')){
                //do nothing
            }else {
                if (this.$(".register-form").parsley('validate')) {
                    this.trigger('onRegisterAttempt');
                } else {
                    // Invalid clientside validations thru parsley
                    if (DEBUG) console.log("Did not pass client side validation");

                }
            }
        },

        render:function () {
            if(app.session.get('logged_in')){
                app.router.navigate('home', { trigger : true, replace : true });
                this.close();
            } else {
                this.template = _.template(RegisterTpl);
                this.$el.html(this.template({
                    logged_in: app.session.get("logged_in"),
                    user: app.session.user.toJSON()
                }));
            }

            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return RegisterSwitchCardView;
});