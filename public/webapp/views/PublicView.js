define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "text!templates/PublicViewTemplate.html",

    'views/AuthenticationSwitchCardView',
    'views/PaperSnakeLogoView',

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    PublicViewTemplate,
    AuthenticationSwitchCardView, PaperSnakeLogoView
){

    var PublicView = Backbone.View.extend({
        tagName: 'div',

        initialize: function () {
            _.bindAll(this);

        },

        events: {

        },


        render:function () {
            this.template = _.template(PublicViewTemplate);

            this.$el.html(this.template({
                user : app.session.user.toJSON()
            }));

            this.afterRender();
            return this;
        },

        afterRender: function(){
            //Setup Login Card
            this.authSwitchCardview = new AuthenticationSwitchCardView();
            this.authSwitchCardview.render();
            this.$('#auth-hook').append(this.authSwitchCardview.el);

            //Setup Register Card
            this.logoView = new PaperSnakeLogoView();
            this.logoView.render();
            this.$('#logo-hook').append(this.logoView.el);

            this.initListeners();
        },

        initListeners:function(){
            this.listenTo(this.authSwitchCardview, 'onLoginStatusChange', this.close);
        },

        close: function(){
            var that = this;

            if(this.authSwitchCardview){
                that.authSwitchCardview.close();
            }

            if(this.logoView){
                that.logoView.close();
            }

            this.remove();
            this.unbind();
        }

    });

    return PublicView;
});