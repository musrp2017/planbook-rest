define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "views/MarketListView",

    "text!templates/MarketAppViewTemplate.html",
    "text!templates/UnderConstructionViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    MarketListView,
    MarketAppViewTpl, UnderConstructionViewTpl
){

    var MarketAppView = Backbone.View.extend({


        initialize: function () {
            _.bindAll(this);

            app.session.on("change:logged_in", this.onLoginStatusChange);

        },

        events: {

        },

        render:function () {
            // this.template = _.template(MarketAppViewTpl); //For ehen the MarketSpp gets implemented, uncomment this line
            this.template = _.template(UnderConstructionViewTpl);

            this.$el.html(this.template({
                user                    : app.session.user.toJSON()
            }));

            this.afterRender();
            return this;
        },

        afterRender: function(){
            var that = this;
            this.innerMarketListView = new MarketListView();
            this.innerMarketListView.render();
            if(this.$('#taskapp')){
                that.$('#taskapp').append(that.innerMarketListView.el);
            }
        },

        onLoginStatusChange: function(evt){
            var that = this;
            if(app.session.get("logged_in")){
                that.render();
            }else{
                that.close();
            }
        },

        close: function(){
            if(this.innerMarketListView){
                this.innerMarketListView.close();
            }
            this.remove();
            this.unbind();
        }

    });

    return MarketAppView;
});