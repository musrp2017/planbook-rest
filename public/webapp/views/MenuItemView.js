define([
    "app",
    "backbone",
    "jquery",
    "underscore",

    "text!templates/MenuItemViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, Backbone, $, _,
    MenuItemTpl
){

    var MenuItemView = Backbone.View.extend({

        id          :   '',
        tagName     :   '',
        className   :   '',

        initialize: function () {
            _.bindAll(this);

            this.model.on('change:isSelected', this.onSelectedChanged);

            this.id = this.model.get("id");
            this.tagName = this.model.get("tagName");
            this.className = this.model.get("className");
        },

        events: {
            'click' : 'highlight'

        },

        render:function () {
            var that = this;
            this.template = _.template(MenuItemTpl);

            this.$el.html(this.template({
                model : that.model.toJSON()
            }));

            return this;
        },

        onSelectedChanged: function() {
            if (this.model.get('isSelected') === true) {
                this.$el.addClass('active');
            }
            else {
                this.$el.removeClass('active');
            }
        },

        highlight: function() {
            this.model.set({isSelected: true});
            this.model.trigger('selected', this.model);
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return MenuItemView;
});