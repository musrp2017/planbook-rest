define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "views/AccordionTaskListView",

    "text!templates/AccordionTaskAppViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    TaskListView,
    TaskAppViewTpl
){

    var AccordionTaskAppView = Backbone.View.extend({


        initialize: function () {
            _.bindAll(this);

            app.session.on("change:logged_in", this.onLoginStatusChange);

        },

        events: {
            'click #btn-newTask'            :   'createNewTask'

        },

        render:function () {
            this.template = _.template(TaskAppViewTpl);

            this.$el.html(this.template({
                user                    : app.session.user.toJSON(),
                priorityNameHigh        : "High",
                priorityNameMedium      : 'Medium',
                priorityNameLow         : 'Low'
            }));

            this.afterRender();
            return this;
        },

        afterRender: function(){
            this.innerTaskListView = new TaskListView();
            this.innerTaskListView.render();

            this.$('.taskapp').append(this.innerTaskListView.el);
        },

        // If you hit return in the main input field, create new **TaskItem** model,
        createNewTask: function (e) {
            this.input = $('#taskTitle');
            this.inputDesc = $('#taskDesc');

            if (!this.input.val().trim()) {
                return;
            }

            this.innerTaskListView.collection.create(this.newAttributes());
            this.closeOverbox();
            this.input.val('');
            this.inputDesc.val('');

        },

        // Generate the default attributes for a new task item.
        newAttributes: function () {
            var that = this;
            this.input = $('#taskTitle');
            this.inputDesc = $('#taskDesc');
            var description = "";
            if(this.inputDesc && this.inputDesc.val().trim() !== ""){
                description = that.inputDesc.val().trim();
            }
            return {
                title: that.input.val().trim(),
                order: that.innerTaskListView.collection.nextOrder(),
                description: description,
                completed: false,
                isNewObj: true
            };
        },

        closeOverbox: function() {
          var closeBtnSelector = $('.shape');
          if(closeBtnSelector){
              closeBtnSelector.click();
          }
        },

        onLoginStatusChange: function(evt){
            var that = this;
            if(app.session.get("logged_in")){
                that.render();
            }else{
                that.close();
            }
        },

        close: function(){
            if(this.innerTaskListView){
                this.innerTaskListView.close();
            }
            this.remove();
            this.unbind();
        }

    });

    return AccordionTaskAppView;
});