define([
    "app",
    "backbone",
    "underscore",

    "text!templates/UserHelpViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, Backbone, _,
    UserHelpTpl
){

    var UserHelpView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.render);
        },

        events: {

        },


        render:function () {
            var that = this;
            if(app.session.get('logged_in')){
                that.template = _.template(UserHelpTpl);
                that.$el.html(this.template({
                    logged_in: app.session.get("logged_in"),
                    user: app.session.user.toJSON()
                }));
            } else {
                app.router.navigate('home', { trigger : true, replace : true });
                that.close();
            }

            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return UserHelpView;
});