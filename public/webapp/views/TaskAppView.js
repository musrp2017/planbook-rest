define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "views/TaskListView",

    "text!templates/TaskAppViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    TaskListView,
    TaskAppViewTpl
){

    var TaskAppView = Backbone.View.extend({


        initialize: function () {
            _.bindAll(this);

            app.session.on("change:logged_in", this.onLoginStatusChange);

        },

        events: {
            "click #btn-newTask": "onNewTask"
        },

        onNewTask:function(){

        },

        render:function () {
            this.template = _.template(TaskAppViewTpl);

            this.$el.html(this.template({
                user                    : app.session.user.toJSON(),
                priorityNameHigh        : "High",
                priorityNameMedium      : 'Medium',
                priorityNameLow         : 'Low'
            }));

            this.afterRender();
            return this;
        },

        afterRender: function(){
            this.innerTaskListView = new TaskListView();
            this.innerTaskListView.render();

            this.$('#taskapp').append(this.innerTaskListView.el);
        },

        onLoginStatusChange: function(evt){
            var that = this;
            if(app.session.get("logged_in")){
                that.render();
            }else{
                that.close();
            }
        },

        close: function(){
            if(this.innerTaskListView){
                this.innerTaskListView.close();
            }
            this.remove();
            this.unbind();
        }

    });

    return TaskAppView;
});