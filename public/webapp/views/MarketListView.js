define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "models/MarketItemCollection",
    "views/MarketItemView",

    "text!templates/TaskListViewTemplate.html",

    "common",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    MarketItemCollection, MarketItemView,
    MarketListViewTpl,
    Common
){

    var MarketListView = Backbone.View.extend({

        itemView: MarketItemView,
        collection: MarketItemCollection,
        items: [],

        initialize: function () {
            _.bindAll(this, 'addSingleItem');

            this.collection.fetch(
                {
                    reset:true,
                    success: function(collection){
                        // This code block will be triggered only after receiving the data.
                        console.log(collection.toJSON());
                    }
                }
            );

            this.setupListeners();

        },

        events: {
            'click #btnStatsFilterCompleted':   'filterAllShowCompleted',
            'click #btnStatsFilterActive'   :   'filterAllShowActive'

        },


        setupListeners: function() {
            this.listenTo(this.collection, 'add', this.addSingleItem, this);
            this.listenTo(this.collection, 'reset', this.addAll, this);
            this.listenTo(this.collection, 'remove', this.removeSingleItem, this);
            this.listenTo(this.collection, 'change:completed', this.filterShowVisible, this);
            this.listenTo(this.collection, 'filter', this.filterAll, this);
            this.listenTo(this.collection, 'change', this.renderStats, this);
        },

        render:function () {
            var that = this;

            this.template = _.template(MarketListViewTpl);

            this.$el.html(that.template({
                user: app.session.user.toJSON()
            }));


            this.addAll();


            return this;
        },

        addSingleItem: function(model) {
            if(this.getViewByModel(model)){
                //Check if the model was already added
                return;
            }
            var viewItem = new this.itemView({
                model: model.toJSON()
            });
            this.items.push(viewItem);
            this.addListItemListeners(viewItem);

            viewItem.render();
            this.$('#market-list').append(viewItem.el);


            return viewItem;
        },

        //propagate list item events through parent list view
        //propagates the single listview and any additional parameters to the listview
        addListItemListeners: function(view) {
            // this.listenTo(view, 'all', function() {
            //     var eventName = 'item:' + arguments[0];
            //     var params = _.toArray(arguments);
            //     params.splice(0,1);
            //     params.unshift(eventName, view);
            //     this.trigger.apply(this, params);
            // });

            // this.listenTo(view.model, 'change', function() {
            //     view.render();
            // });
        },

        removeSingleItem: function(model) {
            var view = this.getViewByModel(model);
            this.removeSingleView(view);
        },

        removeSingleView: function(view) {
            var index;
            //remove listeners
            this.stopListening(view);

            if (view) {
                this.stopListening(view.model);
                view.remove();
                index = this.items.indexOf(view);
                //remove view from items
                this.items.splice(index, 1);
            }
        },

        getViewByModel: function(model) {
            return _.find(this.items, function(item, index) {
                return item.model===model;
            });
        },

        removeAllItems: function() {
            this.collection.each(function(model) {
                this.removeSingleItem(model);
            }, this);
        },

        remove: function() {
            //do the default Backbone remove logic
            Backbone.View.prototype.remove.call(this, arguments);
            //extra remove our items - one by one
            this.removeAllItems();
        },

        // Add all items in the **TaskItemCollection** collection at once.
        addAll: function () {
            //remove previous items if present
            this.removeAllItems();
            //reinit all listeners
            this.setupListeners();
            //add new items
            this.collection.each(function(model) {
                this.addSingleItem(model);
            }, this);
        },

        close: function(){
            this.removeAllItems();
            this.remove();
            this.unbind();
        }

    });

    return MarketListView;
});