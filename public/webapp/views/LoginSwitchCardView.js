define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "text!templates/LoginSwitchCardViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    LoginPageTpl
){

    var LoginView = Backbone.View.extend({
        tagName: 'div',
        className: 'card elevation-3 limit-width log-in-card below turned',


        initialize: function () {
            _.bindAll(this);

            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.render);
        },

        events: {
            'click #login-btn'                      : 'onLoginAttempt',
            'keyup #login-password-input'           : 'onPasswordKeyup'

        },


        // Allow enter press to trigger login
        onPasswordKeyup: function(evt){
            var k = evt.keyCode || evt.which;

            if (k == 13 && $('#login-password-input').val() === ''){
                evt.preventDefault();    // prevent enter-press submit when input is empty
            } else if(k == 13){
                evt.preventDefault();
                this.onLoginAttempt();
                return false;
            }
        },

        onLoginAttempt: function(evt){
            if(evt) evt.preventDefault();
            if($('#login-btn').hasClass('disabled')){
                //do nothing
            }else{
                if(this.$("#login-page-form").parsley('validate')){
                    this.trigger('onLoginAttempt');

                } else {
                    // Invalid clientside validations thru parsley
                    if(DEBUG) console.log("Did not pass clientside validation");

                }
            }

        },

        render:function () {
            if(app.session.get('logged_in')){
                app.router.navigate('home', { trigger : true, replace : false });
            } else {
                this.template = _.template(LoginPageTpl);
            }

            this.$el.html(this.template({
                user: app.session.user.toJSON()
            }));
            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return LoginView;
});