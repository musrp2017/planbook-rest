define([
    "app",

    "text!templates/UserCollectionViewTemplate.html",
    "text!templates/PrivacyViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(app, LoggedInViewTpl, PrivacyTpl){

    var PrivacyView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this, 'onWindowScroll');

            $(window).scroll(this.onWindowScroll);

            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.render);
        },

        events: {
            'click #back-btn'           : 'onBackPress',
            'click #toTop'              : 'onToTopPress'
        },

        onBackPress: function(evt){
            if(evt) evt.preventDefault();
            window.history.back();
        },
        onWindowScroll: function(evt){
            if(evt) evt.preventDefault();
            if ($(window).scrollTop() !== 0) {
                $('#toTop').fadeIn();
                $('#back-btn').fadeOut();

            } else {
                $('#toTop').fadeOut();
                $('#back-btn').fadeIn();

            }
        },
        onToTopPress: function(evt){
            if(evt) evt.preventDefault();
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        },

        render:function () {
            this.template = _.template(PrivacyTpl);
            $(document).scrollTop(0);

            this.$el.html(this.template({ user: app.session.user.toJSON() }));
            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return PrivacyView;
});