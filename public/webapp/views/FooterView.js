define([
    "app", "backbone", "underscore",
    "text!templates/FooterViewTemplate.html",
    "utils",
    "bootstrap"
], function(
    app, Backbone, _,
    FooterTpl
){

    var FooterView = Backbone.View.extend({
        tagName: 'div',
        className: 'container',
        id: 'footer',


        template: _.template(FooterTpl),

        initialize: function () {
            _.bindAll(this);

        },

        events: {
            "click #terms-link"     : "onTermsLinkClick",
            "click #privacy-link"   : "onPrivacyLinkClick"

        },


        onTermsLinkClick: function(evt) {
            if(evt) evt.preventDefault();
            app.router.navigate('termsConditions', { trigger : true, replace : true });
        },

        onPrivacyLinkClick: function(evt) {
            if(evt) evt.preventDefault();
            app.router.navigate('privacyPolicy', { trigger : true, replace : true });
        },

        render: function () {
            this.$el.html(this.template({
                logged_in: app.session.get("logged_in"),
                user: app.session.user.toJSON()
            }));
            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return FooterView;
});