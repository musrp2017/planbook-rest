define([
    "app", "backbone",
    "text!templates/HeaderTemplate.html",
    "utils",
    "bootstrap"
], function(app, Backbone, HeaderTpl){

    var HeaderView = Backbone.View.extend({

        template: _.template(HeaderTpl),

        initialize: function () {
            _.bindAll(this);

            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.onLoginStatusChange);
        },

        events: {
            "click #settings-link"          : "onSettingsClick",
            "click #logout-link"            : "onLogoutClick",
            "click #remove-account-link"    : "onRemoveAccountClick",
            "click #header-login-btn"       : "onLoginAttempt",
            'keyup #header-username-input'  : 'onUsernameKeyUp',
            'keyup #header-password-input'  : 'onPasswordKeyUp'
        },

        onUsernameKeyUp: function(evt){
            var k = evt.keyCode || evt.which;
            if (k == 13 && $('#header-username-input').val() === ''){
                evt.preventDefault();   // prevent enter-press submit when input is empty
            } else if(k == 13){
                evt.preventDefault();
                this.onLoginAttempt(evt);
                return false;
            }
        },

        onPasswordKeyUp: function(evt){
            var k = evt.keyCode || evt.which;
            if (k == 13 && $('#header-password-input').val() === ''){
                evt.preventDefault();   // prevent enter-press submit when input is empty
            } else if(k == 13){
                evt.preventDefault();
                this.onLoginAttempt(evt);
                return false;
            }
        },

        onLoginStatusChange: function(evt){
            this.render();
            if(app.session.get("logged_in")) app.showAlert("Success!", "Logged in as "+app.session.user.get("username"), "alert-success");
            else app.showAlert("Farewell!", "Logged out successfully", "alert-success");
        },

        onLogoutClick: function(evt) {
            evt.preventDefault();
            app.session.logout({});  // No callbacks needed b/c of session event listening
        },

        onSettingsClick: function(evt) {
            if(evt) evt.preventDefault();
            app.router.navigate('settings', { trigger : true, replace : true });
        },

        onLoginAttempt: function(evt){
            if(evt) evt.preventDefault();

            if(this.$("#header-login-form").parsley('validate')){
                app.session.login({
                    username: this.$("#header-username-input").val(),
                    password: this.$("#header-password-input").val()
                }, {
                    success: function(mod, res){
                        if(DEBUG) console.log("SUCCESS", mod, res);

                    },
                    error: function(err){
                        if(DEBUG) console.log("ERROR", err);
                        var errMsg = '';
                        if(typeof err !== 'undefined'){
                            errMsg = err.error;
                        }
                        app.showAlert('The username you’ve entered doesn’t match any account.', errMsg, 'alert-danger');
                    }
                });
            } else {
                // Invalid clientside validations thru parsley
                if(DEBUG) console.log("Did not pass clientside validation");

            }
        },

        onRemoveAccountClick: function(evt){
            evt.preventDefault();
            app.session.removeAccount({});
        },

        render: function () {
            if(DEBUG) console.log("RENDER::", app.session.user.toJSON(), app.session.toJSON());
            this.$el.html(this.template({
                logged_in: app.session.get("logged_in"),
                user: app.session.user.toJSON()
            }));
            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return HeaderView;
});