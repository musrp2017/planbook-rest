define([
    "app",

    "text!templates/UserCollectionViewTemplate.html",
    "text!templates/ForgotPasswordViewTemplate.html",

    "parsley",
    "utils",
    "bootstrap"
], function(app, LoggedInPageTpl, ForgotPasswordTpl){

    var ForgotPasswordView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

            // Listen for session logged_in state changes and re-render
            app.session.on("change:logged_in", this.render);
        },

        events: {
            'click #forgot-pw-submit' : 'onResetPasswordAttempt'
        },

        onResetPasswordAttempt: function(evt){
            if(evt) evt.preventDefault();

            if(this.$("#forgot-pw-form").parsley('validate')){
                app.session.login({
                    username: this.$("#forgot-pw-username-input").val()
                }, {
                    success: function(mod, res){
                        if(DEBUG) console.log("SUCCESS", mod, res);

                    },
                    error: function(err){
                        if(DEBUG) console.log("ERROR", err);
                        var errMsg = '';
                        if(typeof err !== 'undefined'){
                            errMsg = err.error;
                        }
                        app.showAlert('The username you’ve entered doesn’t match any account.', errMsg, 'alert-danger');
                    }
                });
            } else {
                // Invalid clientside validations thru parsley
                if(DEBUG) console.log("Did not pass clientside validation");

            }
        },

        render:function () {
            if(app.session.get('logged_in')){
                app.router.navigate('home', { trigger : true, replace : false });
            } else {
                this.template = _.template(ForgotPasswordTpl);
            }

            this.$el.html(this.template({

                user: app.session.user.toJSON()
            }));
            return this;
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return ForgotPasswordView;
});