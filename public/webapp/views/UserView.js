define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "text!templates/PageTiltMenuViewTemplate.html",

    'views/MenuView',
    'views/UserWelcomeView',
    'views/AccordionTaskAppView',
    'views/MarketAppView',
    'views/UserSidebarView',
    'views/SettingsView',
    'views/UserHelpView',
    'views/UserHomeView',

    "parsley",
    "utils"
], function(
    app, _, Backbone, $,
    PageTiltMenuViewTemplate,
    MenuView, WelcomeView, TaskAppView, MarketAppView, UserSidebarView, SettingsView, UserHelpView, UserHomeView
){

    var UserView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

            app.session.on("change:logged_in", this.onLoginStatusChange);

            this.navHome = $("#nav-home");
            this.navSettings = $('#nav-settings');
            this.navTasks = $('#nav-tasks');
            this.navMarket = $('#nav-market');
            this.navHelp = $('#nav-help');
        },

        events: {
            'click #nav-home'       : 'onClickNavHome',
            'click #nav-tasks'      : 'onClickNavTasks',
            'click #nav-market'     : 'onClickNavMarket',
            'click #nav-settings'   : 'onClickNavSettings',
            'click #nav-help'       : 'onClickNavHelp'
        },


        render:function () {
            this.template = _.template(PageTiltMenuViewTemplate);

            this.$el.html(this.template({
                user : app.session.user.toJSON()
            }));

            this.afterRender();
            return this;
        },

        afterRender: function(){
            this.changeContentView({requestedContent: "Welcome"});
        },

        onClickNavHome:function(evt){
            if(evt) evt.preventDefault();
            this.changeContentView({requestedContent: "Overview"});

            var activeClassName = 'menu__link--active';
            this.navHome.addClass(activeClassName);
            this.checkRemoveClass(this.navTasks, activeClassName);
            this.checkRemoveClass(this.navHelp, activeClassName);
            this.checkRemoveClass(this.navSettings, activeClassName);
            this.checkRemoveClass(this.navMarket, activeClassName);
        },

        onClickNavTasks:function(evt){
            if(evt) evt.preventDefault();
            this.changeContentView({requestedContent: "Tasks"});

            var activeClassName = 'menu__link--active';
            this.navTasks.addClass(activeClassName);
            this.checkRemoveClass(this.navHome, activeClassName);
            this.checkRemoveClass(this.navHelp, activeClassName);
            this.checkRemoveClass(this.navSettings, activeClassName);
            this.checkRemoveClass(this.navMarket, activeClassName);

        },

        onClickNavMarket:function(evt){
            if(evt) evt.preventDefault();
            this.changeContentView({requestedContent: "Market"});

            var activeClassName = 'menu__link--active';
            this.navMarket.addClass(activeClassName);
            this.checkRemoveClass(this.navHome, activeClassName);
            this.checkRemoveClass(this.navHelp, activeClassName);
            this.checkRemoveClass(this.navSettings, activeClassName);
            this.checkRemoveClass(this.navTasks, activeClassName);


        },

        onClickNavSettings:function(evt){
            if(evt) evt.preventDefault();
            this.changeContentView({requestedContent: "Settings"});

            var activeClassName = 'menu__link--active';
            this.navSettings.addClass(activeClassName);
            this.checkRemoveClass(this.navHome, activeClassName);
            this.checkRemoveClass(this.navHelp, activeClassName);
            this.checkRemoveClass(this.navMarket, activeClassName);
            this.checkRemoveClass(this.navTasks, activeClassName);

        },

        onClickNavHelp:function(evt){
            if(evt) evt.preventDefault();
            this.changeContentView({requestedContent: "Help"});

            var activeClassName = 'menu__link--active';
            this.navHelp.addClass(activeClassName);
            this.checkRemoveClass(this.navHome, activeClassName);
            this.checkRemoveClass(this.navMarket, activeClassName);
            this.checkRemoveClass(this.navSettings, activeClassName);
            this.checkRemoveClass(this.navTasks, activeClassName);

        },

        checkRemoveClass:function(jquerySelector, className){
            if(jquerySelector.hasClass(className)){
                jquerySelector.removeClass(className);
            }
        },

        changeContentView:function(evt){
            var that = this;

            var requestedContent = evt.requestedContent;

            if(this.contentView){
                that.contentView.close();
            }

            if(requestedContent){
                if(requestedContent === "Tasks"){
                    that.contentView = new TaskAppView();
                }
                if(requestedContent === "Market"){
                    that.contentView = new MarketAppView();
                }
                if(requestedContent === "Help"){
                    that.contentView = new UserHelpView();
                }
                if(requestedContent === "Settings"){
                    that.contentView = new SettingsView();
                }
                if(requestedContent === "Overview"){
                    that.contentView = new UserHomeView();
                }
                if(requestedContent === "Welcome"){
                    that.contentView = new WelcomeView();
                }
            }
            if(this.contentView){
                that.contentView.render();
                that.$("#content-hook").append(that.contentView.el);
            }
        },

        onLoginStatusChange: function(evt){
            var that = this;
            if(app.session.get("logged_in")){

            }else{
                that.close();
            }
        },

        close: function(){
            var that = this;

            if(this.contentView){
                that.contentView.close();
            }


            this.remove();
            this.unbind();
        }

    });

    return UserView;
});