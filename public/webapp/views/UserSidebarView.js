define([
    "app",
    "underscore",
    "backbone",
    "jquery",

    "text!templates/UserSidebarViewTemplate.html",
    "views/MenuView",
    "models/MenuItemCollection",
    "models/MenuItemModel",

    "parsley",
    "utils",
    "bootstrap"
], function(
    app, _, Backbone, $,
    UserSidebarViewTpl, MenuView, MenuItemCollection, MenuItemModel
){

    var UserSidebarView = Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

            app.session.on("change:logged_in", this.onLoginStatusChange);

        },

        events: {

        },


        render:function () {

            this.template = _.template(UserSidebarViewTpl);

            this.$el.html(this.template({
                user : app.session.user.toJSON()
            }));

            this.afterRender();
            return this;
        },

        afterRender: function(){
            this.menuCollection  = new MenuItemCollection([
                {
                    title       : 'Overview',
                    icon        : 'glyphicon-home',
                    id          : 'sidebar-nav-home'
                },
                {
                    title   : 'Tasks',
                    icon    : 'glyphicon-ok',
                    id      : 'sidebar-nav-task'
                },
                {
                    title   : 'Market',
                    icon    : 'glyphicon-gift',
                    id      : 'sidebar-nav-market'
                },
                {
                    title   : 'Settings',
                    icon    : 'glyphicon-user',
                    id      : 'sidebar-nav-settings'
                },
                {
                    title   : 'Help',
                    icon    : 'glyphicon-flag',
                    id      : 'sidebar-nav-help'
                }

            ]);


            this.menuView = new MenuView({collection: this.menuCollection});

            this.menuView.render();

            this.$('#menu-hook').append(this.menuView.el);

            this.addCollectionListeners();
        },

        addCollectionListeners:function(){
            this.listenTo(this.menuView,  'onSidebarSelectedChange', this.onMenuSelectionChange);
        },

        onMenuSelectionChange:function(evt){
            //raise an event to be picked up by the parent view
            this.trigger('onSidebarSelectedChange', {requestedContent: evt.requestedPage});
        },

        onLoginStatusChange: function(evt){
            var that = this;
            if(app.session.get("logged_in")){
                that.render();
            }else{
                that.close();
            }
        },

        close: function(){
            this.remove();
            this.unbind();
        }

    });

    return UserSidebarView;
});