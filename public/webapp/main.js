/**
 * Main app initialization and initial auth check
 */

require([
        "app",
        "router",
        "models/SessionModel"
    ],
    function(app, WebRouter, SessionModel) {

        // Just use GET and POST to support all browsers
        Backbone.emulateHTTP = true;

        app.router = new WebRouter();

        // Create a new session model and scope it to the app global
        // This will be a singleton, which other modules can access
        app.session = new SessionModel({});

        // Check the auth status upon initialization,
        // before rendering anything or matching routes
        app.session.checkAuth({

            // Start the backbone routing once we have captured a user's auth status
            complete: function(){

                // HTML5 pushState for URLs without hashbangs
                var hasPushstate = !!(window.history && history.pushState);
                if(hasPushstate) Backbone.history.start({ pushState: true, root: app.root });
                else Backbone.history.start();

            }
        });


        // All navigation that is relative should be passed through the navigate
        // method, to be processed by the router. If the link has a `data-bypass`
        // attribute, bypass the delegation completely.
        $(document).on("click", "a:not([data-bypass])", function(evt) {
            // Get the anchor href and protcol
            var href = $(this).attr("href");
            var protocol = this.protocol + "//";

            // Ensure the protocol is not part of URL, meaning its relative.
            if (href && href.slice(0, protocol.length) !== protocol &&
                href.indexOf("javascript:") !== 0) {
                // Stop the default event to ensure the link will not cause a page
                // refresh.
                evt.preventDefault();

                // `Backbone.history.navigate` is sufficient for all Routers and will
                // trigger the correct events.  The Router's internal `navigate` method
                // calls this anyways.
                app.router.navigate(href, {trigger: true, replace: false});
            }
        });
    });