var paperMenu = {
    $window: $('#paper-window'),
    $paperFront: $('#paper-front'),
    $hamburger: $('.hamburger'),
    offset: 1800,
    pageHeight: $('#paper-front').outerHeight(),

    open: function() {
        this.$window.addClass('tilt');
        this.$hamburger.off('click');
        $('#container, .hamburger').on('click', this.close.bind(this));
        this.hamburgerFix(true);
        console.log('opening...');
    },
    close: function() {
        this.$window.removeClass('tilt');
        $('#container, .hamburger').off('click');
        this.$hamburger.on('click', this.open.bind(this));
        this.hamburgerFix(false);
        console.log('closing...');
    },
    updateTransformOrigin: function() {
        var scrollTop = this.$window.scrollTop();
        var equation = (scrollTop + this.offset) / this.pageHeight * 100;

        // console.log('Page Tile DEBUG:');
        // console.log('equation = ' + equation);
        // console.log('scrollTop = ' + scrollTop);
        // console.log('offset = ' + this.offset);
        // console.log('this.pageHeight * 100 = ' + this.pageHeight * 100);

        if(equation > 750){
            //error threshold
            equation = 250;
        }

        this.$paperFront.css('transform-origin', 'center ' + equation + '%');
    },
    //hamburger icon fix to keep its position
    hamburgerFix: function(opening) {
        if(opening) {
            $('.hamburger').css({
                position: 'absolute',
                top: this.$window.scrollTop() + 30 + 'px'
            });
        } else {
            setTimeout(function() {
                $('.hamburger').css({
                    position: 'fixed',
                    top: '10%'
                });
            }, 300);
        }
    },
    bindEvents: function() {
        this.$hamburger.on('click', this.open.bind(this));
        $('.close').on('click', this.close.bind(this));
        this.$window.on('scroll', this.updateTransformOrigin.bind(this));
    },
    init: function() {
        this.bindEvents();
        this.updateTransformOrigin();
    }
};

paperMenu.init();